package by.pavel.che.dao.pool;
import org.apache.log4j.Logger;
import org.junit.Test;

import java.sql.Connection;
import java.sql.SQLException;

import static org.junit.Assert.*;

public class ConnectionPoolTest {

    private static final Logger logger = Logger.getLogger(ConnectionPoolTest.class);

    @Test
    public void closeConnectionPoolTest() {
        ConnectionPool poolToTest = ConnectionPool.getInstance();
        try {
            Connection connection = poolToTest.takeConnection();
            connection.close();
        } catch (SQLException e) {
            logger.error("Cannot close connection\n", e);
            fail("Cannot close connection");
        }
    }

}