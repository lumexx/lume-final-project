package by.pavel.che.util;

import org.junit.Test;

import static org.junit.Assert.*;

public class NullUtilTest {
    @Test
    public void isNullForNullObjectTest() throws Exception {
        assertTrue(NullUtil.isNull(null));
    }

    @Test
    public void isNullForNonNullObjectTest() throws Exception {
        assertFalse(NullUtil.isNull(new Object()));
    }
}