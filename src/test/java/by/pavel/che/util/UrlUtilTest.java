package by.pavel.che.util;

import org.junit.Test;

import static org.junit.Assert.*;

public class UrlUtilTest {

    @Test
    public void removeSeveralParametersFromUrlWithRandomOrderedParametersTest() throws Exception {
        String urlWithoutParameters = "/lume/controller?param2=val2&param1=val1&param3=val3";
        String[] parameterNames = {"param1", "param2", "param3"};

        String expected = "/lume/controller";
        String actual = UrlUtil.removeParametersFromUrl(urlWithoutParameters, parameterNames);
        assertEquals(expected, actual);
    }

    @Test
    public void addOneParameterToUrlWithoutParameters() throws Exception {
        String urlWithoutParameter = "/lume/controller";
        String parameterName = "test1";
        String parameterValue = "test";

        String expected = "/lume/controller?test1=test";
        String actual = UrlUtil.addParameterToUrl(urlWithoutParameter, parameterName, parameterValue);
        assertEquals(expected, actual);
    }

}