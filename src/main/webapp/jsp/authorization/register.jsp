<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:bundle basename="pagecontent" prefix="register.">
<html lang="${sessionScope.locale}">
<head>
    <title>login</title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/bootstrap.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/style.css"/>
</head>
<body>
<jsp:include page="../template/header.jsp"/>
<main class="wrap main_flex__nowrap flex__jcontent_center flex__align-items_center">
    <form action="controller?cmd=register" method="post" id="fileForm" role="form">
        <input type="hidden" name="command" value="signin">
        <div class="form-group">
            <input type="text" name="name" class="form-control"
                   id="txt" onkeyup="Validate(this);"
                   placeholder="<fmt:message key="username"/>">
        </div>
        <div class="form-group">
            <input type="text" name="email" class="form-control"
                   id="email"
                   onchange="email_validate(this.value);"
                   placeholder="<fmt:message key="email"/>">
            <div class="status" id="status"></div>
        </div>
        <div class="form-group">
            <input type="password" name="password" minlength="4"
                   maxlength="16" id="pass1" class="form-control"
                   placeholder="<fmt:message key="password"/>">
        </div>
        <div class="form-group">
            <input type="password" name="password2" minlength="4"
                   maxlength="16" id="pass2"
                   onkeyup="checkPass(); return false;"
                   class="form-control"
                   placeholder="<fmt:message key="repeatPassword"/>">
            <span id="confirmMessage" class="confirmMessage"></span>
        </div>
        <input id="sign_btn"
               class="flex__jcontent_center wrap main_flex__nowrap anime btn flex__align-items_center"
               type="submit" value="<fmt:message key="signin"/>">
    </form>
    <script src="<c:url value="/js/register.js"/>"></script>
</main>
<jsp:include page="../template/footer.jsp"/>
</body>
</html>
</fmt:bundle>