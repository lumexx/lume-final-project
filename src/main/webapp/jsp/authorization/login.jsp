<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:bundle basename="pagecontent" prefix="login.">
<html lang="${sessionScope.locale}">
<head>
    <title>login</title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/bootstrap.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/style.css"/>
</head>
<body>
<jsp:include page="../template/header.jsp"/>
<main class="wrap main_flex__nowrap flex__jcontent_center flex__align-items_center">
    <form action="controller?cmd=login" method="post" id="fileForm" role="form" class="login_form">
        <input type="hidden" name="command" value="signup">
        <div class="form-group">
            <input type="text" name="name" class="form-control" placeholder="<fmt:message key="username"/>">
        </div>
        <div class="form-group">
            <input type="password" name="password" class="form-control"
                   placeholder="<fmt:message key="password"/>">
        </div>
        <input id="sign_btn" class="flex__jcontent_center wrap main_flex__nowrap anime btn flex__align-items_center"
               type="submit" value="<fmt:message key="login"/>">
    </form>
</main>
<jsp:include page="../template/footer.jsp"/>
</body>
</html>
</fmt:bundle>