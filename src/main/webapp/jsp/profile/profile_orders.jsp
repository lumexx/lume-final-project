<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="lx" uri="http://che.pavel.by/lume/tag/lxTags" %>
<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:bundle basename="pagecontent" prefix="profile_orders.">
    <html lang="${sessionScope.locale}">
    <head>
        <title>lume</title>
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/bootstrap.css"/>
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/style.css"/>
    </head>
    <body>
    <jsp:include page="../template/header.jsp"/>
    <main class="wrap main_flex__nowrap flex__jcontent_between flex__align-items_start">
        <div id="left_bar">
            <jsp:include page="template/profile_menu.jsp"/>
        </div>
        <c:choose>
        <c:when test="${not empty sessionScope.user}">
        <div id="right_bar">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th scope="col"><fmt:message key="dateCreated"/></th>
                    <th scope="col"><fmt:message key="datePayment"/></th>
                </tr>
                </thead>
                <tbody>
                <c:choose>
                    <c:when test="${not empty requestScope.orders}">
                        <c:forEach var="order" items="${requestScope.orders}">
                            <tr>
                                <td>${order.dateCreated}</td>
                                <td>${order.dateModification}</td>
                            </tr>
                        </c:forEach>
                    </c:when>
                    <c:otherwise>
                    </c:otherwise>
                </c:choose>
                </tbody>
            </table>
            <div class="row text-center">
                <lx:bootstrapPagination baseUrl="${lx:fullRequestUrl(pageContext.request)}"
                                        currentPage="${requestScope.currentPage}"
                                        pageCount="${requestScope.pageCount}"/>
            </div>
        </div>
        </c:when>
            <c:otherwise>
            </c:otherwise>
        </c:choose>
    </main>
    <jsp:include page="../template/footer.jsp"/>
    </body>
    </html>
</fmt:bundle>