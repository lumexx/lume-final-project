<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:bundle basename="pagecontent" prefix="profile_menu.">
    <div id="category" class="box">
        <div class="list_box">
            <div class="title_box main_flex__nowrap flex__align-items_center flex__jcontent_between">
                <p><fmt:message key="menu"/></p>
            </div>
            <div class="list_link">
                <nav>
                    <li><a href="<c:url value="/controller?cmd=profile_page&name=${sessionScope.user.name}"/>"><fmt:message key="profileInfo"/></a></li>
                    <li><a href="<c:url value="/controller?cmd=profile_orders_page&name=${sessionScope.user.name}"/>"><fmt:message key="orders"/></a>
                    </li>
                </nav>
            </div>
        </div>
    </div>
</fmt:bundle>