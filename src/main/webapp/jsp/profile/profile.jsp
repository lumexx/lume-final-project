<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:bundle basename="pagecontent" prefix="profile.">
    <html lang="${sessionScope.locale}">
    <head>
        <title>lume</title>
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/bootstrap.css"/>
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/style.css"/>
    </head>
    <body>
    <jsp:include page="../template/header.jsp"/>
    <main class="wrap main_flex__nowrap flex__jcontent_between flex__align-items_start">
        <div id="left_bar">
            <jsp:include page="template/profile_menu.jsp"/>
        </div>
        <c:choose>
            <c:when test="${not empty sessionScope.user}">
                <div id="right_bar">
                    <div class="container mb-4">
                        <div class="row">
                            <div class="col-12">
                                <table class="flex__align-items_between flex__jcontent_between main_flex_nowrap admin_table">
                                    <thead>
                                    <tr>
                                        <th scope="col"><fmt:message key="name"/></th>
                                        <th scope="col"><fmt:message key="email"/></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>${sessionScope.user.name}</td>
                                        <td>${sessionScope.user.email}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </c:when>
            <c:otherwise>
            </c:otherwise>
        </c:choose>
    </main>
    <jsp:include page="../template/footer.jsp"/>
    </body>
    </html>
</fmt:bundle>