<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:bundle basename="pagecontent" prefix="header.">
    <header class="dark">
        <div id="top">
            <div class="wrap main_flex__nowrap flex__align-items_center flex__jcontent_between">
                <div class="wrap main_flex__nowrap flex__align-items_center">
                    <div class="wrap main_flex__nowrap flex__align-items_center flex__jcontent_between">
                        <div class=" main_flex__nowrap flex__align-items_start flex__jcontent_between">
                            <c:choose>
                                <c:when test="${not empty sessionScope.user}">
                                    <a class="btn anime logn"
                                       href="<c:url value="/controller?cmd=profile_page&name=${sessionScope.user.name}"/>"><span
                                            class="glyphicon glyphicon-user"></span>${sessionScope.user.name}</a>
                                    <c:if test="${sessionScope.user.role eq 1}">
                                        <a class="btn anime logn"
                                           href="<c:url value="/controller?cmd=admin_page"/>"><span
                                                class="glyphicon glyphicon-cog"></span><fmt:message key="admin"/></a>
                                    </c:if>
                                        <a class="btn anime logn"
                                    href="<c:url value="/controller?cmd=logout"/>"
                                    data-toggle="modal" data-target="#loginModal"><fmt:message key="logout"/></a>
                                </c:when>
                                <c:otherwise>
                                    <a href="<c:url value="/controller?cmd=register_page&from=${requestScope.fullUrl}"/>"
                                       class="sign btn anime" data-toggle="modal" data-target="#SignModal">
                                        <fmt:message key="signin"/>
                                    </a>
                                    <a href="<c:url value="/controller?cmd=login_page&from=${requestScope.fullUrl}"/>"
                                       class="logn btn anime" data-toggle="modal" data-target="#loginModal">
                                        <fmt:message key="login"/>
                                    </a>
                                </c:otherwise>
                            </c:choose>
                        </div>
                        <div class="language_switcher">
                            <a class="language"
                               href="<c:url value="/controller?cmd=set_locale&locale=en&from="/>${requestScope.fullUrl}">EN</a>
                            <strong class="language">|</strong>
                            <a class="language"
                               href="<c:url value="/controller?cmd=set_locale&locale=ru&from="/>${requestScope.fullUrl}">RU</a>
                        </div>
                        <a href="<c:url value="/controller?cmd=cart_page&from=${requestScope.fullUrl}"/>" id="cart"
                           class="cart_btn btn main_flex__nowrap flex__align-items_center flex__jcontent_between anime">
                            <fmt:message key="cart"/>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div id="menu">
            <div class="wrap main_flex__nowrap flex__jcontent_center flex__align-items_center">
                <nav id="main_nav">
                    <li><a href="<c:url value="/controller?cmd=home_page"/>"><fmt:message key="home"/></a></li>
                </nav>
            </div>
        </div>
    </header>
</fmt:bundle>