<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:bundle basename="pagecontent" prefix="footer.">
<footer>
    <div class="wrap">
        <div>
            <nav id="footer_nav" class="main_flex__nowrap flex__jcontent_center flex__align-items_center">
                <li><a href="#"><fmt:message key="legal"/></a></li>
                <li><a href="#"><fmt:message key="terms"/></a></li>
                <li><a href="#"><fmt:message key="contact"/></a></li>
            </nav>
        </div>
    </div>
</footer>
</fmt:bundle>
