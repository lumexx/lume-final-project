<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:bundle basename="pagecontent" prefix="home.">
    <html lang="${sessionScope.locale}">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>lume</title>
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/bootstrap.css"/>
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/style.css"/>
    </head>
    <body>
    <jsp:include page="../template/header.jsp"/>
    <main class="wrap main_flex__nowrap flex__jcontent_between flex__align-items_start">
        <div id="left_bar">
            <jsp:include page="home_menu.jsp"/>
        </div>

        <div id="right_bar">
            <jsp:include page="home_products.jsp"/>
        </div>
    </main>
    <jsp:include page="../template/footer.jsp"/>
    </body>
</fmt:bundle>