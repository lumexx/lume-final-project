<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:bundle basename="pagecontent" prefix="home_menu.">
    <div id="category" class="box">
        <div class="list_box">
            <div class="title_box main_flex__nowrap flex__align-items_center flex__jcontent_between">
                <p><fmt:message key="applications"/></p>
            </div>
            <div class="list_link">
                <nav>
                    <c:choose>
                        <c:when test="${not empty requestScope.categories}">
                            <c:forEach var="category" items="${requestScope.categories}">
                                <li>
                                    <a href="<c:url value="/controller?cmd=home_page&category=${category.id}&from=${requestScope.fullUrl}"/>">${category.name}</a>
                                </li>
                            </c:forEach>
                        </c:when>
                        <c:otherwise>
                        </c:otherwise>
                    </c:choose>
                </nav>
            </div>
        </div>
    </div>
</fmt:bundle>