<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="lx" uri="http://che.pavel.by/lume/tag/lxTags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:bundle basename="pagecontent" prefix="home_products.">
    <div class="list_product main_flex flex__jcontent_start flex__align-items_start">
        <c:choose>
            <c:when test="${not empty requestScope.products}">
                <c:forEach var="product" items="${requestScope.products}">
                    <a href="<c:url value="/controller?cmd=product_page&id=${product.id}&from=${requestScope.fullUrl}"/>"
                       class="product box main_flex__nowrap flex__jcontent_center flex__align-items_center">
                        <div class="img_product">
                            <img src="${pageContext.request.contextPath}/img/${product.image}" alt="">
                        </div>
                        <h2>${product.name}</h2>
                        <p class="price">$${product.price}</p>
                    </a>
                </c:forEach>
            </c:when>
            <c:otherwise>
            </c:otherwise>
        </c:choose>
        <div class="row text-center">
            <lx:bootstrapPagination baseUrl="${lx:fullRequestUrl(pageContext.request)}"
                                    currentPage="${requestScope.currentPage}" pageCount="${requestScope.pageCount}"/>
        </div>
<%--        <button id="load_more" type="button">--%>
<%--            <fmt:message key="loadMore"/>--%>
<%--        </button>--%>
    </div>
</fmt:bundle>