<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:bundle basename="pagecontent" prefix="cart.">
    <html lang="${sessionScope.locale}">
    <head>
        <title>lume</title>
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/bootstrap.css"/>
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/style.css"/>
    </head>
    <body>
    <jsp:include page="template/header.jsp"/>
    <main>
        <div class="container mb-4">
            <div class="row">
                <div class="col-12">
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th scope="col"></th>
                                <th scope="col"><fmt:message key="product"/></th>
                                <th scope="col" class="text-center"><fmt:message key="quantity"/></th>
                                <th scope="col" class="text-right"><fmt:message key="price"/></th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            <c:forEach var="item" items="${sessionScope.cart }">
                                <tr>
                                    <td><img src="${pageContext.request.contextPath}/img/${item.product.image}" width="50"
                                             height="50"/></td>
                                    <td>${item.product.name }</td>
                                    <td>${item.quantity }</td>
                                    <td class="text-right">${item.product.price}$</td>
                                    <td class="text-right">
                                        <a href="<c:url value="/controller?cmd=remove_from_cart&id=${item.product.id}&from=${requestScope.fullUrl}"/>">Remove</a>
                                    </td>
                                </tr>
                            </c:forEach>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col mb-2">
                    <div class="row">
                        <div class="col-sm-12 col-md-6 text-right">
                            <button class="btn btn-lg btn-block btn-success text-uppercase"><a
                                    href="<c:url value="/controller?cmd=checkout&from=${requestScope.fullUrl}"/>"><fmt:message
                                    key="checkout"/></a></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <jsp:include page="template/footer.jsp"/>
    </body>
    </html>
</fmt:bundle>