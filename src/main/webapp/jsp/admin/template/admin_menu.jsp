<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:bundle basename="pagecontent" prefix="admin_menu.">
<div id="category" class="box">
    <div class="list_box">
        <div class="title_box main_flex__nowrap flex__align-items_center flex__jcontent_between">
            <p><fmt:message key="menu"/></p>
        </div>
        <div class="list_link">
            <nav>
                <li><a href="<c:url value="/controller?cmd=admin_page&name=${sessionScope.user.name}"/>"><fmt:message key="profileInfo"/></a></li>
                <li><a href="<c:url value="/controller?cmd=admin_products_page&name=${sessionScope.user.name}"/>"><fmt:message key="products"/></a></li>
                <li><a href="<c:url value="/controller?cmd=admin_users_page&name=${sessionScope.user.name}"/>"><fmt:message key="users"/></a></li>
            </nav>
        </div>
    </div>
</div>
</fmt:bundle>