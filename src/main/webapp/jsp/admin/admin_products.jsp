<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="lx" uri="http://che.pavel.by/lume/tag/lxTags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:bundle basename="pagecontent" prefix="admin_products.">
<html lang="${sessionScope.locale}">
<head>
    <title>lume</title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/bootstrap.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/style.css"/>
</head>
<body>
<jsp:include page="../template/header.jsp"/>
<main class="wrap main_flex__nowrap flex__jcontent_between flex__align-items_start">
    <div id="left_bar">
        <jsp:include page="template/admin_menu.jsp"/>
    </div>
    <div id="right_bar">
        <c:choose>
        <c:when test="${not empty sessionScope.user}">
        <div class="container mb-4">
            <div class="row">
                <div class="col-12">
                    <table class="flex__align-items_between flex__jcontent_between main_flex_nowrap admin_table">
                        <thead>
                        <tr>
                            <th scope="col"></th>
                            <th scope="col"><fmt:message key="image"/></th>
                            <th scope="col"><fmt:message key="title"/></th>
                            <th scope="col"><fmt:message key="price"/></th>
                            <th scope="col" class="text-center"><fmt:message key="description"/></th>
                            <th scope="col" class="text-right"><fmt:message key="category"/></th>
                            <th></th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach var="product" items="${requestScope.products}">
                            <form action="controller?cmd=edit_product&id=${product.id}&from=${requestScope.fullUrl}" method="post" role="form">
                                <tr>
                                    <td><img src="${pageContext.request.contextPath}/img/${product.image}" width="50"
                                             height="50"/></td>
                                    <td><input class="form-control" name="image_url" type="url" placeholder="<fmt:message key="pasteImageUrl"/>"></td>
                                    <td><input required="required" class="form-control" name="product_name" type="text"
                                               value="${product.name }"></td>
                                    <td><input required="required" class="form-control" name="product_price" type="number"
                                               value="${product.price }"></td>
                                    <td><textarea required="required" class="form-control"
                                                  name="product_description">${product.description }</textarea></td>
                                    <td class="text-right"><select name="product_category" class="form-control" id="sel1">
                                        <c:forEach var="category" items="${requestScope.categories}">
                                            <c:choose>
                                                <c:when test="${category.id == product.categoryId}">
                                                    <option value="${category.id}" selected>${category.name}</option>
                                                </c:when>
                                                <c:otherwise>
                                                    <option value="${category.id}">${category.name}</option>
                                                </c:otherwise>
                                            </c:choose>
                                        </c:forEach>
                                    </select></td>
                                    <td class="text-right">
                                        <button type="submit" class="btn btn-secondary"><fmt:message key="save"/></button>
                                    </td>
                                    <td>
                                        <a class="btn btn-danger"
                                           href="<c:url value="/controller?cmd=remove_product&id=${product.id}&from=${requestScope.fullUrl}"/>"><fmt:message key="remove"/></a>
                                    </td>
                                </tr>
                            </form>
                        </c:forEach>
                        <form action="controller?cmd=add_product&from=${requestScope.fullUrl}" method="post" role="form">
                            <tr>
                                <td></td>
                                <td><input class="form-control" name="new_image_url" type="url"
                                           placeholder="<fmt:message key="pasteImageUrl"/>"></td>
                                <td><input class="form-control" name="new_product_name" type="text"
                                           required="required" placeholder="<fmt:message key="productName"/>"></td>
                                <td><input class="form-control" name="new_product_price" type="number"
                                           required="required" placeholder="<fmt:message key="productPrice"/>"></td>
                                <td><textarea required="required" placeholder="<fmt:message key="productDescription"/>" class="form-control"
                                              name="new_product_description">${product.description }</textarea></td>
                                <td class="text-right"><select name="new_product_category" class="form-control">
                                    <c:forEach var="category" items="${requestScope.categories}">
                                        <option value="${category.id}">${category.name}</option>
                                    </c:forEach>
                                </select></td>
                                <td class="text-right">
                                    <button type="submit" class="btn btn-secondary"><fmt:message key="add"/></button>
                                </td>
                            </tr>
                        </form>
                        </tbody>
                    </table>
                    <div class="row text-center">
                        <lx:bootstrapPagination baseUrl="${lx:fullRequestUrl(pageContext.request)}"
                                                currentPage="${requestScope.currentPage}" pageCount="${requestScope.pageCount}"/>
                    </div>
                </div>
            </div>
        </div>
        </c:when>
            <c:otherwise>
            </c:otherwise>
        </c:choose>
    </div>
</main>
<jsp:include page="../template/footer.jsp"/>
</body>
</html>
</fmt:bundle>