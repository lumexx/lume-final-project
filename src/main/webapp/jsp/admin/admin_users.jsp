<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="lx" uri="http://che.pavel.by/lume/tag/lxTags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:bundle basename="pagecontent" prefix="admin_users.">
    <html lang="${sessionScope.locale}">
    <head>
        <title>lume</title>
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/bootstrap.css"/>
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/style.css"/>
    </head>
    <body>
    <jsp:include page="../template/header.jsp"/>
    <main class="wrap main_flex__nowrap flex__jcontent_between flex__align-items_start">
        <div id="left_bar">
            <jsp:include page="template/admin_menu.jsp"/>
        </div>
        <div id="right_bar">
            <c:choose>
                <c:when test="${not empty sessionScope.user}">
                    <div class="container mb-4">
                        <div class="row">
                            <div class="col-12">
                                <table class="flex__align-items_between flex__jcontent_between main_flex_nowrap admin_table">
                                    <thead>
                                    <tr>
                                        <th scope="col"><fmt:message key="name"/></th>
                                        <th scope="col"><fmt:message key="email"/></th>
                                        <th scope="col" class="text-center"><fmt:message key="status"/></th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <c:forEach var="user" items="${requestScope.users}">
                                        <c:if test="${sessionScope.user.name ne user.name}">
                                            <form action="controller?cmd=edit_user&id=${user.id}&from=${requestScope.fullUrl}"
                                                  method="post" role="form">
                                                <tr>
                                                    <td><input required="required" class="form-control" name="user_name" type="text"
                                                               value="${user.name }"></td>
                                                    <td><input required="required" class="form-control" name="user_email" type="text"
                                                               value="${user.email }"></td>
                                                    <td class="text-right"><select name="user_role" class="form-control"
                                                                                   id="sel1">
                                                        <c:choose>
                                                            <c:when test="${user.role == 1}">
                                                                <option value="1" selected><fmt:message
                                                                        key="admin"/></option>
                                                                <option value="0"><fmt:message key="user"/></option>
                                                            </c:when>
                                                            <c:otherwise>
                                                                <option value="1"><fmt:message key="admin"/></option>
                                                                <option value="0" selected><fmt:message
                                                                        key="user"/></option>
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </select></td>
                                                    <td class="text-right">
                                                        <button type="submit" class="btn btn-secondary"><fmt:message
                                                                key="save"/></button>
                                                    </td>
                                                    <td class="text-right">
                                                        <a class="btn btn-danger"
                                                           href="<c:url value="/controller?cmd=remove_user&id=${user.id}&from=${requestScope.fullUrl}"/>"><fmt:message
                                                                key="remove"/></a>
                                                    </td>
                                                </tr>
                                            </form>
                                        </c:if>
                                    </c:forEach>
                                    </tbody>
                                </table>
                                <div class="row text-center">
                                    <lx:bootstrapPagination baseUrl="${lx:fullRequestUrl(pageContext.request)}"
                                                            currentPage="${requestScope.currentPage}"
                                                            pageCount="${requestScope.pageCount}"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </c:when>
                <c:otherwise>
                </c:otherwise>
            </c:choose>
        </div>
    </main>
    <jsp:include page="../template/footer.jsp"/>
    </body>
    </html>
</fmt:bundle>