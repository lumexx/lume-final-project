<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:bundle basename="pagecontent" prefix="product.">
    <html lang="${sessionScope.locale}">
    <head>
        <title>lume</title>
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/bootstrap.css"/>
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/style.css"/>
    </head>
    <body>
    <jsp:include page="template/header.jsp"/>
    <main class="wrap main_flex__nowrap flex__jcontent_between flex__align-items_start">
        <div class="card">
            <img class="card-img-top img-fluid" src="${pageContext.request.contextPath}/img/${product.image}" alt="">
            <div class="card-body">
                <h3 class="card-title">${product.name}</h3>
                <h4>$${product.price}</h4>
                <p class="card-text">${product.description}</p>
                <a href="<c:url value="/controller?cmd=add_to_cart&id=${product.id}"/>"><fmt:message key="buy"/></a>
            </div>
        </div>
    </main>
    <jsp:include page="template/footer.jsp"/>
    </body>
    </html>
</fmt:bundle>