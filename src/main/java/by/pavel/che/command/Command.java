package by.pavel.che.command;

import by.pavel.che.exception.command.CommandException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Main interface of Command pattern which used to delegate
 * every web app action from controller to separated amount of classes
 */
public interface Command {

    /**
     * Default dispatch for request
     *
     * @param pagePath path of page to forward
     * @param request  request to dispatch
     * @param response response to dispatch
     * @throws CommandException if cannot dispatch request for specified {@code pagePath}
     */
    static void dispatchRequest(String pagePath, HttpServletRequest request,
                                HttpServletResponse response) throws CommandException {
        try {
            response.setCharacterEncoding("UTF-8");
            request.getRequestDispatcher(pagePath).forward(request, response);
        } catch (ServletException e) {
            throw new CommandException("Servlet exception occurs. ", e);
        } catch (IOException e) {
            throw new CommandException("IOException exception occurs. ", e);
        }
    }

    /**
     * Default redirect for the specified url
     *
     * @param url      url to redirect to
     * @param response response for redirecting
     * @throws CommandException if internal error occurs
     */
    static void sendRedirect(String url, HttpServletResponse response) throws CommandException {
        try {
            url = response.encodeRedirectURL(url);
            response.sendRedirect(url);
        } catch (IOException e) {
            throw new CommandException("IOException exception occurs. ", e);
        }
    }

    /**
     * Executes command using {@link HttpServletRequest} specified
     *
     * @param request request from the user with necessary data to execute
     * @param response response to the user for future actions
     * @throws CommandException if command cannot be executed or execution failed
     */
    void execute(HttpServletRequest request, HttpServletResponse response) throws CommandException;
}
