package by.pavel.che.command.res;

public class Constants {
    public static final String COMMAND_PARAMETER = "cmd";
    public static final String FROM_URL_PARAMETER = "from";
    public static final String FULL_URL_ATTRIBUTE = "fullUrl";
    public static final String LOCALE_ATTRIBUTE = "locale";
    public static final String USER_ATTRIBUTE = "user";
    public static final String CART_ATTRIBUTE = "cart";


    public static final String INDEX_PAGE_PATH = "index.jsp";
    public static final String CART_PAGE_PATH = "/jsp/cart.jsp";
    public static final String CHECKOUT_SUCCESS_PATH = "/jsp/checkout_success.jsp";
    public static final String ADMIN_PAGE_PATH = "/jsp/admin/admin.jsp";
    public static final String ADMIN_PRODUCTS_PAGE_PATH = "/jsp/admin/admin_products.jsp";
    public static final String ADMIN_USERS_PAGE_PATH = "/jsp/admin/admin_users.jsp";
    public static final String LOGIN_PAGE_PATH = "/jsp/authorization/login.jsp";
    public static final String REGISTER_PAGE_PATH = "/jsp/authorization/register.jsp";
    public static final String HOME_PAGE_PATH = "/jsp/home/home.jsp";
    public static final String PROFILE_PAGE_PATH = "/jsp/profile/profile.jsp";
    public static final String PROFILE_ORDERS_PAGE_PATH = "/jsp/profile/profile_orders.jsp";
    public static final String PRODUCT_PAGE_PATH = "/jsp/product.jsp";


    public static final String USERS_LIST_ATTRIBUTE = "users";
    public static final String ORDERS_LIST_ATTRIBUTE = "orders";
    public static final String PRODUCTS_LIST_ATTRIBUTE = "products";
    public static final String CATEGORIES_LIST_ATTRIBUTE = "categories";
    public static final String PAGE_INDEX_PARAMETER = "page";
    public static final String PAGE_COUNT_ATTRIBUTE = "pageCount";
    public static final String CURRENT_PAGE_ATTRIBUTE = "currentPage";

}
