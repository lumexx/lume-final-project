package by.pavel.che.command.factory;

import by.pavel.che.command.Command;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

import static by.pavel.che.command.res.Constants.COMMAND_PARAMETER;
import static by.pavel.che.util.StringUtil.isNullOrEmpty;

public class CommandFactory {

    private static final Logger logger = Logger.getLogger(CommandFactory.class);

    /**
     * Defines Concrete {@link Command} implementation based on
     * COMMAND_PARAMETER in {@link HttpServletRequest} specified.
     * {@link CommandEnum} used for mapping
     *
     * @param request request for concrete command implementation
     * @return Concrete command implementation
     */
    public static Command defineCommand(HttpServletRequest request) {
        String actionName = request.getParameter(COMMAND_PARAMETER);
        Command resultCommand = null;
        if (!isNullOrEmpty(actionName)) {
            try {
                resultCommand = CommandEnum.valueOf(actionName.toUpperCase()).getCommand();
            } catch (IllegalArgumentException e) {
                logger.warn("Command with specified name not found. (name: " + actionName + ")");
            }
        } else {
            if (actionName == null) {
                resultCommand = CommandEnum.HOME_PAGE.getCommand();
            } else {
                logger.warn("Command name is empty (value: \"" + actionName + "\")");
            }
        }

        return resultCommand;
    }
}
