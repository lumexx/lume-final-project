package by.pavel.che.command.factory;

import by.pavel.che.command.Command;
import by.pavel.che.command.impl.*;

/**
 * Represents full list of commands, used in factory
 */
public enum CommandEnum {
    /**
     * This command provides page with registration form
     */
    REGISTER_PAGE(new GetRegisterPageCommand()),
    /**
     * This command provides home page
     */
    HOME_PAGE(new GetHomePageCommand()),
    /**
     * This command provides product page
     */
    PRODUCT_PAGE(new GetProductPageCommand()),
    /**
     * This command provides page with login form
     */
    LOGIN_PAGE(new GetLoginPageCommand()),
    /**
     * This command provides cart page
     */
    CART_PAGE(new GetCartPageCommand()),
    /**
     * Register new user on site
     */
    REGISTER(new RegisterCommand()),
    /**
     * Authorizes user on site
     */
    LOGIN(new LoginCommand()),
    /**
     * Logs out user from the site
     */
    LOGOUT(new LogoutCommand()),
    /**
     * Adding new product to cart
     */
    ADD_TO_CART(new AddToCartCommand()),
    /**
     * Removing product from cart
     */
    REMOVE_FROM_CART(new RemoveFromCartCommand()),
    /**
     * Paying for all products in cart
     */
    CHECKOUT(new CheckoutCommand()),
    /**
     * This command provides profile page
     */
    PROFILE_PAGE(new GetProfilePageCommand()),
    /**
     * This command provides profile orders page
     */
    PROFILE_ORDERS_PAGE(new GetProfileOrdersPageCommand()),
    /**
     * This command provides admin page
     */
    ADMIN_PAGE(new GetAdminPageCommand()),
    /**
     * This command provides admin product page
     */
    ADMIN_PRODUCTS_PAGE(new GetAdminProductsPageCommand()),
    /**
     * Removes product from database
     */
    REMOVE_PRODUCT(new RemoveProductCommand()),
    /**
     * Removes user from database
     */
    REMOVE_USER(new RemoveUserCommand()),
    /**
     * Adds product to database
     */
    ADD_PRODUCT(new AddProductCommand()),
    /**
     * Edit product information
     */
    EDIT_PRODUCT(new EditProductCommand()),
    /**
     * Edit user data
     */
    EDIT_USER(new EditUserCommand()),
    /**
     * This command provides admin users page
     */
    ADMIN_USERS_PAGE(new GetAdminUsersPageCommand()),
    /**
     * Changes website language
     */
    SET_LOCALE(new SetLocaleCommand());

    private Command command;

    CommandEnum(Command command) {
        this.command = command;
    }

    public Command getCommand() {
        return command;
    }
}
