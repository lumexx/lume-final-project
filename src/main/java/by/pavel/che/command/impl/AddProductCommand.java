package by.pavel.che.command.impl;

import by.pavel.che.command.Command;
import by.pavel.che.entity.Product;
import by.pavel.che.entity.User;
import by.pavel.che.exception.command.CommandException;
import by.pavel.che.exception.service.ServiceException;
import by.pavel.che.service.ProductService;
import by.pavel.che.service.factory.ServiceFactory;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;

import static by.pavel.che.command.res.Constants.USER_ATTRIBUTE;
import static by.pavel.che.util.NullUtil.isNull;
import static by.pavel.che.util.StringUtil.isNullOrEmpty;
import static by.pavel.che.util.UrlUtil.getBackRedirectUrl;

public class AddProductCommand implements Command {

    private static final Logger logger = Logger.getLogger(AddProductCommand.class);

    private static final String IMAGE_URL_PARAMETER = "new_image_url";
    private static final String NAME_PARAMETER = "new_product_name";
    private static final String DESCRIPTION_PARAMETER = "new_product_description";
    private static final String PRICE_PARAMETER = "new_product_price";
    private static final String CATEGORY_ID_PARAMETER = "new_product_category";

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        String imageUrlParameter = request.getParameter(IMAGE_URL_PARAMETER);
        String nameParameter = request.getParameter(NAME_PARAMETER);
        String descriptionParameter = request.getParameter(DESCRIPTION_PARAMETER);
        long priceParameter = Long.parseLong(request.getParameter(PRICE_PARAMETER));
        long categoryIdParameter = Long.parseLong(request.getParameter(CATEGORY_ID_PARAMETER));
        String destPath = request.getServletContext()
                .getRealPath("/")
                .replace("target\\lume-1.0-SNAPSHOT\\", "")
                + "src\\main\\webapp\\img\\";
        try {
            HttpSession session = request.getSession();
            User user = (User) session.getAttribute(USER_ATTRIBUTE);
            if (!isNull(user)) {
                ProductService productService = ServiceFactory.getInstance().getProductService();
                Product product = new Product();
                if (user.getRole() == User.ROLE_ADMIN) {
                    if (!isNullOrEmpty(imageUrlParameter)) {
                        product.setImage(uploadImage(imageUrlParameter, destPath));
                    }
                    product.setName(nameParameter);
                    product.setDescription(descriptionParameter);
                    product.setPrice(priceParameter);
                    product.setCategoryId(categoryIdParameter);
                    productService.add(product);
                } else {
                    logger.warn("User '" + user.getName() +
                            "' trying to add product without permission.");
                }
            } else {
                logger.warn("Unauthorized user trying to edit product.\n");
            }
        } catch (ServiceException e) {
            throw new CommandException(e);
        } catch (IOException e) {
            logger.warn("Invalid image url parameter passed (" + imageUrlParameter + ")");
        }
        Command.sendRedirect(getBackRedirectUrl(request), response);
    }

    private static String uploadImage(String inputImage, String destPath) throws IOException {
        URL url = new URL(inputImage);
        String fileName = url.getFile().substring(url.getFile().lastIndexOf("/") + 1);
        String destName = destPath + fileName;
        InputStream is = url.openStream();
        OutputStream os = new FileOutputStream(destName);
        byte[] b = new byte[2048];
        int length;
        while ((length = is.read(b)) != -1) {
            os.write(b, 0, length);
        }
        is.close();
        os.close();
        return fileName;
    }
}
