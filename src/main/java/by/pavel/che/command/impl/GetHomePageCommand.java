package by.pavel.che.command.impl;

import by.pavel.che.command.Command;
import by.pavel.che.entity.Category;
import by.pavel.che.entity.Product;
import by.pavel.che.exception.command.CommandException;
import by.pavel.che.exception.service.ServiceException;
import by.pavel.che.service.CategoryService;
import by.pavel.che.service.ItemPageService;
import by.pavel.che.service.ProductService;
import by.pavel.che.service.factory.ServiceFactory;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.List;

import static by.pavel.che.command.res.Constants.*;
import static by.pavel.che.service.ItemPageService.ITEMS_PER_PAGE;
import static by.pavel.che.util.StringUtil.isNullOrEmpty;
import static by.pavel.che.util.UrlUtil.getServletUrl;

public class GetHomePageCommand implements Command {

    private static final Logger logger = Logger.getLogger(GetHomePageCommand.class);

    private static final String PAGE_PARAMETER = "page";
    private static final String CATEGORY_PARAMETER = "category";

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        String pageParameter = request.getParameter(PAGE_PARAMETER);
        String categoryParameter = request.getParameter(CATEGORY_PARAMETER);
        int pageIndex = 1;
        if (!isNullOrEmpty(pageParameter)) {
            try {
                pageIndex = Integer.parseInt(pageParameter);
            } catch (NumberFormatException e) {
                logger.trace("Invalid page parameter passed (" + pageParameter + ")");
            }
        }
        ItemPageService.ItemsPage<Product> productsPage;
        List<Category> categories;
        try {
            ProductService productService = ServiceFactory.getInstance().getProductService();
            if (isNullOrEmpty(categoryParameter)) {
                productsPage = productService.getPage(pageIndex, ITEMS_PER_PAGE);
            } else {
                long catId = Long.parseLong(categoryParameter);
                productsPage = productService.getPageForCategory(pageIndex, ITEMS_PER_PAGE, catId);
            }
            CategoryService categoryService = ServiceFactory.getInstance().getCategoryService();
            categories = categoryService.getAllCategories();
        } catch (ServiceException e) {
            throw new CommandException(e);
        }
        request.setAttribute(CATEGORIES_LIST_ATTRIBUTE, categories);
        request.setAttribute(CURRENT_PAGE_ATTRIBUTE, productsPage.getCurrentIndex());
        request.setAttribute(PRODUCTS_LIST_ATTRIBUTE, productsPage.getItems());
        request.setAttribute(PAGE_COUNT_ATTRIBUTE , productsPage.getPageCount());

        Command.dispatchRequest(HOME_PAGE_PATH, request, response);
    }
}
