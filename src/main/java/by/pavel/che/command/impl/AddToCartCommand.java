package by.pavel.che.command.impl;

import by.pavel.che.command.Command;
import by.pavel.che.entity.CartItem;
import by.pavel.che.entity.Product;
import by.pavel.che.exception.command.CommandException;
import by.pavel.che.exception.service.ServiceException;
import by.pavel.che.service.ProductService;
import by.pavel.che.service.factory.ServiceFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

import static by.pavel.che.command.res.Constants.CART_ATTRIBUTE;
import static by.pavel.che.command.res.Constants.INDEX_PAGE_PATH;
import static by.pavel.che.util.UrlUtil.getBackRedirectUrl;

public class AddToCartCommand implements Command {

    private static final String PRODUCT_ID_PARAMETER = "id";

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        String prodIdParameter = request.getParameter(PRODUCT_ID_PARAMETER);
        try {
            HttpSession session = request.getSession();
            long pid = Long.parseLong(prodIdParameter);
            try {
                ProductService productService = ServiceFactory.getInstance().getProductService();
                Product product = productService.getById(pid);
                if (session.getAttribute(CART_ATTRIBUTE) == null) {
                    List<CartItem> cart = new ArrayList<CartItem>();
                    cart.add(new CartItem(product, 1));
                    session.setAttribute(CART_ATTRIBUTE, cart);
                } else {
                    List<CartItem> cart = (List<CartItem>) session.getAttribute(CART_ATTRIBUTE);
                    int index = isExisting(pid, cart);
                    if (index == -1) {
                        cart.add(new CartItem(product, 1));
                    } else {
                        int quantity = cart.get(index).getQuantity() + 1;
                        cart.get(index).setQuantity(quantity);
                    }
                    session.setAttribute(CART_ATTRIBUTE, cart);
                }
            } catch (ServiceException e) {
                throw new CommandException(e);
            }
        } catch (NumberFormatException e) {
        }
        Command.sendRedirect(getBackRedirectUrl(request), response);
    }

    private int isExisting(long id, List<CartItem> cart) {
        for (int i = 0; i < cart.size(); i++) {
            if (cart.get(i).getProduct().getId() == id) {
                return i;
            }
        }
        return -1;
    }
}
