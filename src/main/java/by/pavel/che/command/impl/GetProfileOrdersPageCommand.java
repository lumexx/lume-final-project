package by.pavel.che.command.impl;

import by.pavel.che.command.Command;
import by.pavel.che.entity.Order;
import by.pavel.che.entity.User;
import by.pavel.che.exception.command.CommandException;
import by.pavel.che.exception.service.ServiceException;
import by.pavel.che.service.ItemPageService;
import by.pavel.che.service.OrderService;
import by.pavel.che.service.UserService;
import by.pavel.che.service.factory.ServiceFactory;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import static by.pavel.che.command.res.Constants.*;
import static by.pavel.che.util.NullUtil.isNull;
import static by.pavel.che.util.StringUtil.isNullOrEmpty;

public class GetProfileOrdersPageCommand implements Command {

    private static final Logger logger = Logger.getLogger(GetProfileOrdersPageCommand.class);

    private static final String USERNAME_PARAMETER = "name";
    private static final String PAGE_PARAMETER = "page";
    private static final String ORDER_COUNT_ATTRIBUTE = "orderCount";

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        String usernameParameter = request.getParameter(USERNAME_PARAMETER);
        String pageParameter = request.getParameter(PAGE_PARAMETER);

        int pageIndex = 0;
        if (!isNullOrEmpty(pageParameter)) {
            try {
                pageIndex = Integer.parseInt(pageParameter);
            } catch (NumberFormatException e) {
                logger.trace("Invalid page parameter passed (" + pageParameter + ")");
            }
        }

        if (!isNullOrEmpty(usernameParameter)) {
            HttpSession session = request.getSession();
            User loggedUser = (User) session.getAttribute(USER_ATTRIBUTE);
            User user;
            if (!isNull(loggedUser) && usernameParameter.equals(loggedUser.getName())) {
                user = loggedUser;
            } else {
                try {
                    UserService userService = ServiceFactory.getInstance().getUserService();
                    user = userService.getByUsername(usernameParameter);
                } catch (ServiceException e) {
                    throw new CommandException(e);
                }
            }

            if (!isNull(user)) {
                ItemPageService.ItemsPage<Order> ordersPage;
                int ordersCount;
                try {
                    OrderService orderService = ServiceFactory.getInstance().getOrderService();
                    ordersPage = orderService.getPageForUser(user.getName(), pageIndex);
                    ordersCount = orderService.getUserOrderCount(user.getName());
                } catch (ServiceException e) {
                    throw new CommandException(e);
                }
                request.setAttribute(USER_ATTRIBUTE, user);
                request.setAttribute(ORDER_COUNT_ATTRIBUTE, ordersCount);
                request.setAttribute(ORDERS_LIST_ATTRIBUTE, ordersPage.getItems());
                request.setAttribute(PAGE_COUNT_ATTRIBUTE, ordersPage.getPageCount());
                request.setAttribute(CURRENT_PAGE_ATTRIBUTE, ordersPage.getCurrentIndex());
            }
        }
        response.setHeader("Cache-control","no-store");
        response.setHeader("Pragma","no-cache");
        response.setDateHeader("Expires", -1);
        Command.dispatchRequest(PROFILE_ORDERS_PAGE_PATH, request, response);
    }
}
