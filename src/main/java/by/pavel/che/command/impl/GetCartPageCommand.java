package by.pavel.che.command.impl;

import by.pavel.che.command.Command;
import by.pavel.che.exception.command.CommandException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static by.pavel.che.command.res.Constants.CART_PAGE_PATH;

public class GetCartPageCommand implements Command {

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        Command.dispatchRequest(CART_PAGE_PATH, request, response);
    }
}
