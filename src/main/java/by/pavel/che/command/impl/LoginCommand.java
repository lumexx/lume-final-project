package by.pavel.che.command.impl;

import by.pavel.che.command.Command;
import by.pavel.che.entity.User;
import by.pavel.che.exception.command.CommandException;
import by.pavel.che.exception.service.ServiceException;
import by.pavel.che.service.UserService;
import by.pavel.che.service.factory.ServiceFactory;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import static by.pavel.che.command.res.Constants.*;
import static by.pavel.che.util.NullUtil.isNull;
import static by.pavel.che.util.UrlUtil.getBackRedirectUrl;

public class LoginCommand implements Command {
    private static final Logger logger = Logger.getLogger(LoginCommand.class);
    private static final String USERNAME_PARAMETER = "name";
    private static final String PASSWORD_PARAMETER = "password";

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        String username = request.getParameter(USERNAME_PARAMETER);
        String password = request.getParameter(PASSWORD_PARAMETER);
        User user = null;
        try {
            HttpSession session = request.getSession();
            user = (User) session.getAttribute(USER_ATTRIBUTE);
            if (isNull(user)) {
                UserService userService = ServiceFactory.getInstance().getUserService();
                user = userService.login(username, password);
                session.setAttribute(USER_ATTRIBUTE, user);
                session.setAttribute(LOCALE_ATTRIBUTE, user.getLocale());
                logger.debug("User \"" + username + "\" logged in");
            } else {
                logger.debug("User trying to login twice without logout.");
            }
        } catch (ServiceException e) {
            logger.debug("Exception in user service.");
        }
        if (isNull(user)) {
            Command.dispatchRequest(LOGIN_PAGE_PATH, request, response);
        } else {
            Command.sendRedirect(getBackRedirectUrl(request), response);
        }

    }
}
