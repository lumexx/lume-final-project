package by.pavel.che.command.impl;

import by.pavel.che.command.Command;
import by.pavel.che.entity.Product;
import by.pavel.che.exception.command.CommandException;
import by.pavel.che.exception.service.ServiceException;
import by.pavel.che.service.ProductService;
import by.pavel.che.service.factory.ServiceFactory;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static by.pavel.che.command.res.Constants.*;
import static by.pavel.che.util.NullUtil.isNull;

public class GetProductPageCommand implements Command {

    private static final Logger logger = Logger.getLogger(GetProductPageCommand.class);
    private static final String PRODUCT_ATTRIBUTE = "product";
    private static final String PRODUCT_ID_PARAMETER = "id";

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        String prodIdParameter = request.getParameter(PRODUCT_ID_PARAMETER);
        String pagePath = INDEX_PAGE_PATH;
        try {
            long quid = Long.parseLong(prodIdParameter);
            try {
                ProductService productService = ServiceFactory.getInstance().getProductService();
                Product product = productService.getById(quid);
                if (!isNull(product)) {
                    request.setAttribute(PRODUCT_ATTRIBUTE, product);
                    pagePath = PRODUCT_PAGE_PATH;
                }
            } catch (ServiceException e) {
                throw new CommandException(e);
            }
        } catch (NumberFormatException e) {
            logger.warn("Invalid product ID parameter passed (" + prodIdParameter + ")");
        }
        Command.dispatchRequest(pagePath,  request, response);
    }
}
