package by.pavel.che.command.impl;

import by.pavel.che.command.Command;
import by.pavel.che.exception.command.CommandException;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static by.pavel.che.util.UrlUtil.getBackRedirectUrl;

public class LogoutCommand implements Command {

    private static final Logger logger = Logger.getLogger(LogoutCommand.class);

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        request.getSession().removeAttribute("user");
        logger.debug("User logged out");
        Command.sendRedirect(getBackRedirectUrl(request), response);
    }
}