package by.pavel.che.command.impl;

import by.pavel.che.command.Command;
import by.pavel.che.entity.User;
import by.pavel.che.exception.command.CommandException;
import by.pavel.che.exception.service.ServiceException;
import by.pavel.che.service.UserService;
import by.pavel.che.service.factory.ServiceFactory;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import static by.pavel.che.command.res.Constants.LOCALE_ATTRIBUTE;
import static by.pavel.che.command.res.Constants.USER_ATTRIBUTE;
import static by.pavel.che.util.UrlUtil.getBackRedirectUrl;

public class SetLocaleCommand implements Command {

    private static final Logger logger = Logger.getLogger(SetLocaleCommand.class);
    private static final String LOCALE_PARAMETER = "locale";

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        String newLocale = request.getParameter(LOCALE_PARAMETER);
        HttpSession session = request.getSession();
        session.setAttribute(LOCALE_ATTRIBUTE, newLocale);
        User user = (User) session.getAttribute(USER_ATTRIBUTE);
        if (user != null) {
            UserService userService = ServiceFactory.getInstance().getUserService();
            user.setLocale(newLocale);
            try {
                userService.update(user);
            } catch (ServiceException e) {
                logger.error("Cannot store user locale: \n", e);
            }
        }
        logger.debug("Locale was set to \"" + newLocale + "\"");
        Command.sendRedirect(getBackRedirectUrl(request), response);
    }
}
