package by.pavel.che.command.impl;

import by.pavel.che.command.Command;
import by.pavel.che.dao.UserDAO;
import by.pavel.che.dao.factory.DAOFactory;
import by.pavel.che.dao.factory.MySqlDAOFactory;
import by.pavel.che.dao.impl.MySqlUserDAO;
import by.pavel.che.entity.User;
import by.pavel.che.exception.command.CommandException;
import by.pavel.che.exception.dao.DAOException;
import by.pavel.che.exception.service.ServiceException;
import by.pavel.che.service.UserService;
import by.pavel.che.service.factory.ServiceFactory;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import static by.pavel.che.command.res.Constants.USER_ATTRIBUTE;
import static by.pavel.che.util.NullUtil.isNull;
import static by.pavel.che.util.UrlUtil.getBackRedirectUrl;

public class EditUserCommand implements Command {

    private static final Logger logger = Logger.getLogger(EditProductCommand.class);

    private static final String NAME_PARAMETER = "user_name";
    private static final String EMAIL_PARAMETER = "user_email";
    private static final String ROLE_PARAMETER = "user_role";
    private static final String USER_ID_PARAMETER = "id";

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        String nameParameter = request.getParameter(NAME_PARAMETER);
        String emailParameter = request.getParameter(EMAIL_PARAMETER);
        int roleParameter = Integer.parseInt(request.getParameter(ROLE_PARAMETER));
        String idParameter = request.getParameter(USER_ID_PARAMETER);
        try {
            long id = Long.parseLong(idParameter);
            HttpSession session = request.getSession();
            User user = (User) session.getAttribute(USER_ATTRIBUTE);
            if (!isNull(user)) {
                UserService userService = ServiceFactory.getInstance().getUserService();
                User userUpd = userService.getById(id);
                if (!isNull(userUpd)) {
                    if (user.getRole() == User.ROLE_ADMIN) {
                        userUpd.setName(nameParameter);
                        userUpd.setEmail(emailParameter);
                        userUpd.setRole(roleParameter);
                        userService.update(userUpd);
                    } else {
                        logger.warn("User '" + user.getName() +
                                "' trying to edit user (id=" + idParameter + ") without permission.");
                    }
                } else {
                    logger.warn("User not found (id=" + idParameter + ")");
                }
            } else {
                logger.warn("Unauthorized user trying to edit user.\n");
            }
        } catch (NumberFormatException e) {
            logger.warn("Invalid user ID parameter passed (" + idParameter + ")");
        } catch (ServiceException e) {
            throw new CommandException(e);
        }
        Command.sendRedirect(getBackRedirectUrl(request), response);
    }
}
