package by.pavel.che.command.impl;

import by.pavel.che.command.Command;
import by.pavel.che.entity.CartItem;
import by.pavel.che.entity.Order;
import by.pavel.che.entity.Purchase;
import by.pavel.che.entity.User;
import by.pavel.che.exception.command.CommandException;
import by.pavel.che.exception.service.ServiceException;
import by.pavel.che.service.OrderService;
import by.pavel.che.service.PurchaseService;
import by.pavel.che.service.factory.ServiceFactory;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

import static by.pavel.che.command.res.Constants.*;
import static by.pavel.che.util.NullUtil.isNull;
import static by.pavel.che.util.UrlUtil.getBackRedirectUrl;

public class CheckoutCommand implements Command {

    private static final Logger logger = Logger.getLogger(CheckoutCommand.class);

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        HttpSession session = request.getSession();
        boolean success = false;
        if(!(null == session.getAttribute(CART_ATTRIBUTE)))
        {
            List<CartItem> cart = (List<CartItem>) session.getAttribute(CART_ATTRIBUTE);
            User user = (User) session.getAttribute(USER_ATTRIBUTE);
            if (!isNull(user)) {
                if (cart.size() != 0) {
                    OrderService orderService = ServiceFactory.getInstance().getOrderService();
                    Order order = new Order();
                    order.setUserId(user.getId());
                    order.setStatus(0);
                    PurchaseService purchaseService = ServiceFactory.getInstance().getPurchaseService();
                    Purchase purchase = new Purchase();
                    try {
                        order = orderService.add(order);
                        logger.debug("Checkout added \n" + order);
                        for (CartItem product : cart) {
                            purchase.setOrderId(order.getId());
                            purchase.setProductId(product.getProduct().getId());
                            purchase.setAmount(product.getQuantity());
                            purchase.setPrice((int) product.getProduct().getPrice());
                            purchase = purchaseService.add(purchase);
                            success = true;
                            request.getSession().removeAttribute(CART_ATTRIBUTE);
                        }
                    } catch (ServiceException e) {
                        throw new CommandException(e);
                    }
                }
            }
        }
        if (success) {
            Command.dispatchRequest(CHECKOUT_SUCCESS_PATH, request, response);
        } else {
            Command.sendRedirect(getBackRedirectUrl(request), response);
        }
    }
}
