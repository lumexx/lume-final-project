package by.pavel.che.command.impl;

import by.pavel.che.command.Command;
import by.pavel.che.entity.User;
import by.pavel.che.exception.command.CommandException;
import by.pavel.che.exception.service.ServiceException;
import by.pavel.che.service.UserService;
import by.pavel.che.service.factory.ServiceFactory;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import static by.pavel.che.command.res.Constants.USER_ATTRIBUTE;
import static by.pavel.che.util.NullUtil.isNull;
import static by.pavel.che.util.UrlUtil.getBackRedirectUrl;

public class RemoveUserCommand implements Command {

    private static final Logger logger = Logger.getLogger(RemoveUserCommand.class);
    private static final String USER_ID_PARAMETER = "id";

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        String idParameter = request.getParameter(USER_ID_PARAMETER);
        try {
            long aid = Long.parseLong(idParameter);
            HttpSession session = request.getSession();
            User user = (User) session.getAttribute(USER_ATTRIBUTE);
            if (!isNull(user)) {
                UserService userService = ServiceFactory.getInstance().getUserService();
                User userDel = userService.getById(aid);
                if (!isNull(userDel)) {
                    if (user.getRole() == User.ROLE_ADMIN) {
                        userService.delete(userDel.getId());
                        logger.debug("User deleted: \n" + userDel);
                    } else {
                        logger.warn("User '" + user.getName() +
                                "' trying to delete user (id=" + idParameter + ") without permission.");
                    }
                } else {
                    logger.warn("User not found (id=" + idParameter + ")");
                }
            } else {
                logger.warn("Unauthorized user trying to delete user.\n" +
                        "(userID=" + idParameter + ")");
            }
        } catch (NumberFormatException e) {
            logger.warn("Invalid user ID parameter passed (" + idParameter + ")");
        } catch (ServiceException e) {
            throw new CommandException(e);
        }
        Command.sendRedirect(getBackRedirectUrl(request), response);
    }
}
