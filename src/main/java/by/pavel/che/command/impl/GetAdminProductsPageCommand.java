package by.pavel.che.command.impl;

import by.pavel.che.command.Command;
import by.pavel.che.entity.Category;
import by.pavel.che.entity.Product;
import by.pavel.che.entity.User;
import by.pavel.che.exception.command.CommandException;
import by.pavel.che.exception.service.ServiceException;
import by.pavel.che.service.CategoryService;
import by.pavel.che.service.ItemPageService;
import by.pavel.che.service.ProductService;
import by.pavel.che.service.factory.ServiceFactory;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

import static by.pavel.che.command.res.Constants.*;
import static by.pavel.che.service.ProductService.PRODUCTS_PER_PAGE;
import static by.pavel.che.util.NullUtil.isNull;
import static by.pavel.che.util.StringUtil.isNullOrEmpty;

public class GetAdminProductsPageCommand implements Command {

    private static final Logger logger = Logger.getLogger(GetAdminProductsPageCommand.class);

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute(USER_ATTRIBUTE);
        String pagePath = INDEX_PAGE_PATH;
        if (!isNull(user)) {
            if (user.getRole() == User.ROLE_ADMIN) {
                String pageParameter = request.getParameter(PAGE_INDEX_PARAMETER);
                pagePath = ADMIN_PRODUCTS_PAGE_PATH;
                int pageIndex = 1;
                if (!isNullOrEmpty(pageParameter)) {
                    try {
                        pageIndex = Integer.parseInt(pageParameter);
                    } catch (NumberFormatException e) {
                        logger.trace("Invalid page parameter passed (" + pageParameter + ")");
                    }
                }

                ItemPageService.ItemsPage<Product> productsPage;
                List<Category> categories;
                try {
                    ProductService productService = ServiceFactory.getInstance().getProductService();
                    productsPage = productService.getPage(pageIndex, PRODUCTS_PER_PAGE);
                    CategoryService categoryService = ServiceFactory.getInstance().getCategoryService();
                    categories = categoryService.getAllCategories();

                } catch (ServiceException e) {
                    throw new CommandException(e);
                }
                request.setAttribute(CATEGORIES_LIST_ATTRIBUTE, categories);
                request.setAttribute(CURRENT_PAGE_ATTRIBUTE, productsPage.getCurrentIndex());
                request.setAttribute(PRODUCTS_LIST_ATTRIBUTE, productsPage.getItems());
                request.setAttribute(PAGE_COUNT_ATTRIBUTE, productsPage.getPageCount());
            }
        }
        response.setHeader("Cache-control","no-store");
        response.setHeader("Pragma","no-cache");
        response.setDateHeader("Expires", -1);
        Command.dispatchRequest(pagePath, request, response);
    }
}
