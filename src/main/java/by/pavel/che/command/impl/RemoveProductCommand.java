package by.pavel.che.command.impl;

import by.pavel.che.command.Command;
import by.pavel.che.entity.Product;
import by.pavel.che.entity.User;
import by.pavel.che.exception.command.CommandException;
import by.pavel.che.exception.service.ServiceException;
import by.pavel.che.service.ProductService;
import by.pavel.che.service.factory.ServiceFactory;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import static by.pavel.che.command.res.Constants.USER_ATTRIBUTE;
import static by.pavel.che.util.NullUtil.isNull;
import static by.pavel.che.util.UrlUtil.getBackRedirectUrl;

public class RemoveProductCommand implements Command {

    private static final Logger logger = Logger.getLogger(RemoveProductCommand.class);
    private static final String PRODUCT_ID_PARAMETER = "id";

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        String idParameter = request.getParameter(PRODUCT_ID_PARAMETER);
        try {
            long aid = Long.parseLong(idParameter);
            HttpSession session = request.getSession();
            User user = (User) session.getAttribute(USER_ATTRIBUTE);
            if (!isNull(user)) {
                ProductService productService = ServiceFactory.getInstance().getProductService();
                Product product = productService.getById(aid);
                if (!isNull(product)) {
                    if (user.getRole() == User.ROLE_ADMIN) {
                        productService.delete(product.getId());
                        logger.debug("Product deleted: \n" + product);
                    } else {
                        logger.warn("User '" + user.getName() +
                                "' trying to delete product (id=" + idParameter + ") without permission.");
                    }
                } else {
                    logger.warn("Product not found (id=" + idParameter + ")");
                }
            } else {
                logger.warn("Unauthorized user trying to delete answer.\n" +
                        "(productID=" + idParameter + ")");
            }
        } catch (NumberFormatException e) {
            logger.warn("Invalid product ID parameter passed (" + idParameter + ")");
        } catch (ServiceException e) {
            throw new CommandException(e);
        }
        Command.sendRedirect(getBackRedirectUrl(request), response);
    }
}
