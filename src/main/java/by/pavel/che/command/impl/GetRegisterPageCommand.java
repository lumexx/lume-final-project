package by.pavel.che.command.impl;

import by.pavel.che.command.Command;
import by.pavel.che.entity.User;
import by.pavel.che.exception.command.CommandException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import static by.pavel.che.command.res.Constants.*;
import static by.pavel.che.command.res.Constants.LOGIN_PAGE_PATH;
import static by.pavel.che.util.NullUtil.isNull;

public class GetRegisterPageCommand implements Command {

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute(USER_ATTRIBUTE);
        String pagePath = INDEX_PAGE_PATH;
        if (isNull(user)) {
            pagePath = REGISTER_PAGE_PATH;
        }
        Command.dispatchRequest(pagePath, request, response);
    }
}
