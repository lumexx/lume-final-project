package by.pavel.che.command.impl;

import by.pavel.che.command.Command;
import by.pavel.che.entity.User;
import by.pavel.che.exception.command.CommandException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import static by.pavel.che.command.res.Constants.*;
import static by.pavel.che.util.NullUtil.isNull;

public class GetAdminPageCommand implements Command {

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute(USER_ATTRIBUTE);
        String pagePath = INDEX_PAGE_PATH;
        if (!isNull(user)) {
            if (user.getRole() == User.ROLE_ADMIN) {
                pagePath = ADMIN_PAGE_PATH;
            }
        }
        response.setHeader("Cache-control","no-store");
        response.setHeader("Pragma","no-cache");
        response.setDateHeader("Expires", -1);
        Command.dispatchRequest(pagePath, request, response);
    }
}
