package by.pavel.che.command.impl;

import by.pavel.che.command.Command;

import by.pavel.che.exception.command.CommandException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static by.pavel.che.command.res.Constants.PROFILE_PAGE_PATH;


public class GetProfilePageCommand implements Command {

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        response.setHeader("Cache-control","no-store");
        response.setHeader("Pragma","no-cache");
        response.setDateHeader("Expires", -1);
        Command.dispatchRequest(PROFILE_PAGE_PATH, request, response);
    }
}
