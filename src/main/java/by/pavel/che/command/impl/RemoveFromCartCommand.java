package by.pavel.che.command.impl;

import by.pavel.che.command.Command;
import by.pavel.che.entity.CartItem;
import by.pavel.che.exception.command.CommandException;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

import static by.pavel.che.command.res.Constants.CART_ATTRIBUTE;
import static by.pavel.che.util.UrlUtil.getBackRedirectUrl;

public class RemoveFromCartCommand implements Command {

    private static final String CART_ID_PARAMETER = "id";
    private static final Logger logger = Logger.getLogger(RemoveFromCartCommand.class);

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        HttpSession session = request.getSession();
        List<CartItem> cart = (List<CartItem>) session.getAttribute(CART_ATTRIBUTE);
        int index = isExisting(request.getParameter(CART_ID_PARAMETER), cart);
        if (index != -1) {
            cart.remove(index);
        } else {
            logger.warn("Product not found");
        }
        session.setAttribute(CART_ATTRIBUTE, cart);
        Command.sendRedirect(getBackRedirectUrl(request), response);
    }

    private int isExisting(String id, List<CartItem> cart) {
        for (int i = 0; i < cart.size(); i++) {
            if (cart.get(i).getProduct().getId() == Long.parseLong(id)) {
                return i;
            }
        }
        return -1;
    }

}
