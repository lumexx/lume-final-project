package by.pavel.che.command.impl;

import by.pavel.che.command.Command;
import by.pavel.che.entity.User;
import by.pavel.che.exception.command.CommandException;
import by.pavel.che.exception.service.ServiceException;
import by.pavel.che.exception.service.UserServiceException;
import by.pavel.che.service.UserService;
import by.pavel.che.service.factory.ServiceFactory;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import static by.pavel.che.command.res.Constants.REGISTER_PAGE_PATH;
import static by.pavel.che.command.res.Constants.USER_ATTRIBUTE;
import static by.pavel.che.util.NullUtil.isNull;
import static by.pavel.che.util.UrlUtil.getBackRedirectUrl;

public class RegisterCommand implements Command {

    private static final Logger logger = Logger.getLogger(RegisterCommand.class);
    private static final String USERNAME_PARAMETER = "name";
    private static final String EMAIL_PARAMETER = "email";
    private static final String PASSWORD_PARAMETER = "password";

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        String username = request.getParameter(USERNAME_PARAMETER);
        String email = request.getParameter(EMAIL_PARAMETER);
        String password = request.getParameter(PASSWORD_PARAMETER);
        User user = null;

        try {
            HttpSession session = request.getSession();
            UserService userService = ServiceFactory.getInstance().getUserService();
            user = userService.register(username, email, password);
            session.setAttribute(USER_ATTRIBUTE, user);
        } catch (UserServiceException e) {
            logger.debug("Registration failed for (" + username + ", " + email + ")");
        } catch (ServiceException e) {
            throw new CommandException(e);
        }

        if (isNull(user)) {
            Command.dispatchRequest(REGISTER_PAGE_PATH, request, response);
        } else {
            Command.sendRedirect(getBackRedirectUrl(request), response);
        }
    }
}
