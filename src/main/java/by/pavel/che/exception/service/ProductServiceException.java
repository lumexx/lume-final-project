package by.pavel.che.exception.service;

/**
 * Thrown to indicate that something wrong with product logic
 */
public class ProductServiceException extends ServiceException {

    public ProductServiceException() {
        super();
    }

    public ProductServiceException(String cause) {
        super(cause);
    }

    public ProductServiceException(Throwable t) {
        super(t);
    }

    public ProductServiceException(String cause, Throwable t) {
        super(cause, t);
    }
}
