package by.pavel.che.exception.service;

/**
 * Thrown to indicate that something wrong with category logic
 */
public class CategoryServiceException extends ServiceException {

    public CategoryServiceException() {
        super();
    }

    public CategoryServiceException(String cause) {
        super(cause);
    }

    public CategoryServiceException(Throwable t) {
        super(t);
    }

    public CategoryServiceException(String cause, Throwable t) {
        super(cause, t);
    }
}
