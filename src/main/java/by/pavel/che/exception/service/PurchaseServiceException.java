package by.pavel.che.exception.service;

/**
 * Thrown to indicate that something wrong with purchase logic
 */
public class PurchaseServiceException extends ServiceException {

    public PurchaseServiceException() {
        super();
    }

    public PurchaseServiceException(String cause) {
        super(cause);
    }

    public PurchaseServiceException(Throwable t) {
        super(t);
    }

    public PurchaseServiceException(String cause, Throwable t) {
        super(cause, t);
    }
}
