package by.pavel.che.exception.service;

/**
 * Thrown to indicate that something wrong with order logic
 */
public class OrderServiceException extends ServiceException {

    public OrderServiceException() {
        super();
    }

    public OrderServiceException(String cause) {
        super(cause);
    }

    public OrderServiceException(Throwable t) {
        super(t);
    }

    public OrderServiceException(String cause, Throwable t) {
        super(cause, t);
    }
}
