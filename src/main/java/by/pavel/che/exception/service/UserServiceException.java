package by.pavel.che.exception.service;

/**
 * Thrown to indicate that something wrong with user logic
 */
public class UserServiceException extends ServiceException {

    public UserServiceException() {
        super();
    }

    public UserServiceException(String cause) {
        super(cause);
    }

    public UserServiceException(Throwable t) {
        super(t);
    }

    public UserServiceException(String cause, Throwable t) {
        super(cause, t);
    }

}
