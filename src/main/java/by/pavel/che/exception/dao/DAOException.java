package by.pavel.che.exception.dao;

/**
 * Exception which represents errors in work with database and data
 */
public class DAOException extends Exception {

    public DAOException() {
        super();
    }

    public DAOException(String cause) {
        super(cause);
    }

    public DAOException(Throwable t) {
        super(t);
    }

    public DAOException(String cause, Throwable t) {
        super(cause, t);
    }

}