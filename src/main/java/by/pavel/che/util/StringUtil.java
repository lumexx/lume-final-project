package by.pavel.che.util;

import static by.pavel.che.util.NullUtil.isNull;

/**
 * Util class for string operations
 */
public final class StringUtil {

    private StringUtil() {
    }

    /**
     * Checks is specified string {@code null} or has no significant char
     *
     * @param string string to test
     * @return {@code true} if {@code null} or empty, false otherwise
     */
    public static boolean isNullOrEmpty(String string) {
        return (isNull(string)) || (string.trim().isEmpty());
    }

}
