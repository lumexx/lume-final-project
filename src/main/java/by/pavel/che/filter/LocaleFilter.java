package by.pavel.che.filter;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static by.pavel.che.command.res.Constants.LOCALE_ATTRIBUTE;
import static by.pavel.che.util.NullUtil.isNull;

/**
 * Filter used to initialize default locale
 */
@WebFilter(urlPatterns = {"/*"}, initParams = {
        @WebInitParam(name = "defaultLocale", value = "ru"),
        @WebInitParam(name = "supportedLocales", value = "en,ru")
})
public class LocaleFilter implements Filter {

    private String defaultLocale;
    private List<String> supportedLocales;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        defaultLocale = filterConfig.getInitParameter("defaultLocale");
        supportedLocales = Arrays.asList(filterConfig.getInitParameter("supportedLocales").split(","));
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpSession session = ((HttpServletRequest) servletRequest).getSession();
        String locale = (String) session.getAttribute(LOCALE_ATTRIBUTE);
        if (isNull(locale)) {
            String browserLocale = servletRequest.getLocale().getLanguage();
            if (supportedLocales.contains(browserLocale)) {
                session.setAttribute(LOCALE_ATTRIBUTE, browserLocale);
            } else {
                session.setAttribute(LOCALE_ATTRIBUTE, defaultLocale);
            }
        }
        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {
        defaultLocale = null;
    }
}