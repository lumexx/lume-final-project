package by.pavel.che.entity;

/**
 * Represents table `category` in database
 */
public class Category extends Entity {

    private String name;

    public Category() {
    }

    public Category(long id, long parentId, String name) {
        super(id);

        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Category{" +
                "id=" + getId() +
                ", name='" + name + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Category category = (Category) o;

        return super.equals(o) &&
                name.equals(category.getName());
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + name.hashCode();
        return result;
    }
}
