package by.pavel.che.entity;

import static by.pavel.che.util.NullUtil.nullableEquals;
import static by.pavel.che.util.NullUtil.nullableHashCode;
/**
 * Represents table `user` in database
 */
public class User extends Entity {
    /**
     * Represents that this User entity is Client
     */
    public static final int ROLE_CLIENT = 0;
    /**
     * Represents that this User entity is Admin
     */
    public static final int ROLE_ADMIN = 1;

    private String name;
    private String email;
    private String password;
    private int role;
    private String locale;

    public User() {
    }

    public User(long id, String name, String email, String password, int role, String locale) {
        super(id);
        this.name = name;
        this.email = email;
        this.password = password;
        this.role = role;
        this.locale = locale;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getRole() {
        return role;
    }

    public void setRole(int role) {
        this.role = role;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        return super.equals(o) &&
                email.equals(user.getEmail()) &&
                name.equals(user.getName()) &&
                password.equals(user.getPassword()) &&
                role == user.role &&
                nullableEquals(locale, user.getLocale());
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + getId() +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", locale='" + locale + '\'' +
                ", role=" + role +
                '}';
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 17 * result + email.hashCode();
        result = 31 * result + name.hashCode();
        result = 17 * result + password.hashCode();
        result = 31 * result + role;
        result = 17 * result + nullableHashCode(locale.hashCode());
        return result;
    }
}
