package by.pavel.che.entity;

import java.sql.Timestamp;

import static by.pavel.che.util.NullUtil.nullableEquals;
import static by.pavel.che.util.NullUtil.nullableHashCode;

/**
 * Represents table `order` in database
 */
public class Order extends Entity {
    private long userId;
    private Timestamp dateCreated;
    private Timestamp datePayment;
    private Timestamp dateModification;
    private int status;

    public Order() {
    }

    public Order(long id, long userId, Timestamp dateCreated, Timestamp datePayment, Timestamp dateModification, int status, String comment, String userIp) {
        super(id);
        this.userId = userId;
        this.dateCreated = dateCreated;
        this.datePayment = datePayment;
        this.dateModification = dateModification;
        this.status = status;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public Timestamp getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Timestamp dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Timestamp getDatePayment() {
        return datePayment;
    }

    public void setDatePayment(Timestamp datePayment) {
        this.datePayment = datePayment;
    }

    public Timestamp getDateModification() {
        return dateModification;
    }

    public void setDateModification(Timestamp dateModification) {
        this.dateModification = dateModification;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Order order = (Order) o;

        return super.equals(o) &&
                userId == order.userId &&
                dateCreated == order.dateCreated &&
                nullableEquals(datePayment, order.getDatePayment()) &&
                dateModification == order.dateModification &&
                status == order.status;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (int) (userId ^ (userId >>> 32));
        result = 17 * result + dateCreated.hashCode();
        result = 31 * result + nullableHashCode(datePayment);
        result = 17 * result + dateModification.hashCode();
        result = 31 * result + status;
        return result;
    }

    @Override
    public String toString() {
        return "Order{" +
                "id=" + getId() +
                ", userId=" + userId +
                ", dateCreated=" + dateCreated +
                ", datePayment=" + datePayment +
                ", dateModification=" + dateModification +
                ", status=" + status +
                '}';
    }
}
