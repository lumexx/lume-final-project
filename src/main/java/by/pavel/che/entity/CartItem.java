package by.pavel.che.entity;

/**
 * Represents item in shopping cart
 */
public class CartItem  {
    private Product product;
    private int quantity;

    public CartItem() {
    }

    public CartItem(Product product, int quantity) {
        this.product = product;
        this.quantity = quantity;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CartItem cartItem = (CartItem) o;

        return super.equals(o) &&
                product.equals(cartItem.getProduct()) &&
                quantity == cartItem.quantity;
    }

    @Override
    public String toString() {
        return "CartItem{" +
                "product=" + product.toString() +
                ", quantity=" + quantity +
                '}';
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 17 * result + product.hashCode();
        result = 31 * result + quantity;
        return result;
    }

}