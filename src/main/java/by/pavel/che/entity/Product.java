package by.pavel.che.entity;

import static by.pavel.che.util.NullUtil.nullableEquals;
import static by.pavel.che.util.NullUtil.nullableHashCode;

/**
 * Represents table `product` in database
 * plus category name which product belongs to
 */
public class Product extends Entity {

    private long categoryId;
    private String name;
    private String description;
    private long price;
    private String image;

    private String categoryName;

    public Product() {

    }

    public Product(long id, long categoryId, String name, String description, long price, String imageName, int status, String categoryName) {
        super(id);
        this.categoryId = categoryId;
        this.name = name;
        this.description = description;
        this.price = price;
        this.image = imageName;
        this.categoryName = categoryName;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String creatorUsername) {
        this.categoryName = creatorUsername;
    }

    public long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(long categoryId) {
        this.categoryId = categoryId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getPrice() {
        return price;
    }

    public void setPrice(long price) {
        this.price = price;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String imageName) {
        this.image = imageName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Product product = (Product) o;

        return super.equals(o) &&
                categoryId == product.getCategoryId() &&
                name.equals(product.getName()) &&
                description.equals(product.getDescription()) &&
                price == product.price &&
                image.equals(product.getImage()) &&
                nullableEquals(categoryName, product.categoryName);
    }


    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 17 * result + (int) (categoryId ^ (categoryId >>> 32));
        result = 31 * result + name.hashCode();
        result = 17 * result + description.hashCode();
        result = 31 * result + (int) (price ^ (price >>> 32));
        result = 17 * result + image.hashCode();
        result = 17 * result + nullableHashCode(categoryName);
        return result;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + getId() +
                "categoryId=" + categoryId +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", price=" + price +
                ", imageName='" + image + '\'' +
                ", categoryName=" + categoryName +
                '}';
    }
}
