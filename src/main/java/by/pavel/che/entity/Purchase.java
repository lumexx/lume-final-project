package by.pavel.che.entity;

/**
 * Represents table `purchase` in database
 */
public class Purchase extends Entity {

    private long orderId;
    private long productId;
    private int price;
    private int amount;

    public Purchase(){}

    public Purchase(long id, long orderId, long productId, int price, int amount) {
        super(id);
        this.orderId = orderId;
        this.productId = productId;
        this.price = price;
        this.amount = amount;
    }

    public long getOrderId() {
        return orderId;
    }

    public void setOrderId(long orderId) {
        this.orderId = orderId;
    }

    public long getProductId() {
        return productId;
    }

    public void setProductId(long productId) {
        this.productId = productId;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Purchase purchase = (Purchase) o;

        return super.equals(o) &&
                orderId == purchase.orderId &&
                productId == purchase.productId &&
                price == purchase.price &&
                amount == purchase.amount;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (int) (orderId ^ (orderId >>> 32));
        result = 17 * result + (int) (productId ^ (productId >>> 32));
        result = 31 * result + price;
        result = 17 * result + amount;
        return result;
    }

    @Override
    public String toString() {
        return "Purchase{" +
                "id=" + getId() +
                "orderId=" + orderId +
                ", productId=" + productId +
                ", price=" + price +
                ", amount=" + amount +
                '}';
    }
}

