package by.pavel.che.service;

import by.pavel.che.entity.User;
import by.pavel.che.exception.service.ServiceException;

/**
 * Service which provides all needed user methods.
 */
public interface UserService extends ItemPageService<User> {

    /**
     * Default users count on one page
     */
    int USERS_PER_PAGE = 10;

    /**
     * Creates new user with the specified credentials
     *
     * @param username username of user, must be unique
     * @param email    email of user, must be unique
     * @param password password of new user
     * @return {@link User} object which represents registered user
     * @throws ServiceException     if error happens during execution
     */
    User register(String username, String email, String password) throws ServiceException;

    /**
     * Authorizes user with the given credentials.
     *
     * @param username username of a user
     * @param password password of a user
     * @return {@link User} entity of authorized user.
     * @throws ServiceException     if error happens during execution
     */
    User login(String username, String password) throws ServiceException;

    /**
     * Returns {@link User} with the specified ID
     *
     * @param id id of a question
     * @return {@link User} with the specified ID, {@code null} if such question is not exist
     * @throws ServiceException if error happens during execution
     */
    User getById(long id) throws ServiceException;

    /**
     * Returns user with specified username, or {@code null}
     * if user not found.<br>
     *
     * @param username username of a user to get
     * @return {@link User} entity with specified username or {@code null} if entity not found
     * @throws ServiceException if error happens during execution
     */
    User getByUsername(String username) throws ServiceException;

    /**
     * Updates user data<br>
     *
     * @param updatedUser updated user entity which will be stored into database
     * @throws ServiceException if error happens during execution
     */
    void update(User updatedUser) throws ServiceException;

    /**
     * Deletes user with the specified id from the system.<br>
     * Does nothing if user with this id doesn't exist.
     *
     * @param id of the {@link User} to delete
     * @throws ServiceException if error happens during execution
     */
    void delete(long id) throws ServiceException;

}
