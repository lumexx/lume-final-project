package by.pavel.che.service;

import by.pavel.che.entity.Category;

import by.pavel.che.exception.service.ServiceException;

import java.util.List;

/**
 * Service which provides all needed category methods.
 */
public interface CategoryService {

    /** Selects all existing categories from database
     *
     * @return list of {@link Category}
     * @throws ServiceException if error happens during execution
     */
    List<Category> getAllCategories() throws ServiceException;
}
