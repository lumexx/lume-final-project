package by.pavel.che.service;


import by.pavel.che.entity.Order;

import by.pavel.che.exception.service.ServiceException;

/**
 * Service which provides all needed order methods.
 */
public interface OrderService  extends ItemPageService<Order> {

    /**
     * Adds new product
     *
     * @param order product
     * @return {@link Order} entity with all filled properties
     * @throws ServiceException       if error happens during execution
     */
    Order add(Order order) throws ServiceException;

    /**
     * Counts how many orders created user with such username
     *
     * @param username username of a user
     * @return user orders count
     * @throws ServiceException if error happens during execution
     */
    int getUserOrderCount(String username) throws ServiceException;

    /**
     * Selects page with specified index of orders which created user with such username
     *
     * @param username username of a user
     * @param index index of the page
     * @return {@link ItemsPage} entity of the current page
     * @throws ServiceException if error happens during execution
     */
    ItemsPage<Order> getPageForUser(String username, int index) throws ServiceException;
}
