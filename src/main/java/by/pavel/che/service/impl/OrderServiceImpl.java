package by.pavel.che.service.impl;

import by.pavel.che.dao.OrderDAO;
import by.pavel.che.dao.factory.DAOFactory;
import by.pavel.che.entity.Order;
import by.pavel.che.exception.service.ServiceException;
import by.pavel.che.service.OrderService;

import java.util.List;

import static by.pavel.che.util.caller.DAOCaller.tryCallDAO;

public class OrderServiceImpl implements OrderService {

    private OrderDAO getOrderDAO() {
        return DAOFactory.getDAOFactory().getOrderDAO();
    }

    @Override
    public Order add(Order order) throws ServiceException {
        return tryCallDAO(() -> getOrderDAO().insert(order));
    }

    @Override
    public int getUserOrderCount(String username) throws ServiceException {
        return tryCallDAO(() -> getOrderDAO().selectCountByUsername(username));
    }

    private static int calculatePageCount(int orderCount, int itemCount) {
        int pageCount;
        if ((orderCount % itemCount == 0) && (orderCount != 0)) {
            pageCount = orderCount / itemCount;
        } else {
            pageCount = orderCount / itemCount + 1;
        }
        return pageCount;
    }

    @Override
    public ItemsPage<Order> getPageForUser(String username, int index) throws ServiceException {
        if ((index - 1) * ITEMS_PER_PAGE < 0) {
            index = 1;
        }
        int offset = (index - 1) * ITEMS_PER_PAGE;
        List<Order> items = tryCallDAO(() -> getOrderDAO().selectByUsernameWithLimit(username, offset, ITEMS_PER_PAGE));
        int orderCount = tryCallDAO(() -> getOrderDAO().selectCountByUsername(username));
        int pageCount = calculatePageCount(orderCount, ITEMS_PER_PAGE);
        return new ItemsPage<>(items, pageCount, index);
    }

    @Override
    public ItemsPage<Order> getPage(int index, int itemsPerPage) throws ServiceException {
        return null;
    }
}
