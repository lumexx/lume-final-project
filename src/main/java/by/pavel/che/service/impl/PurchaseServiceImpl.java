package by.pavel.che.service.impl;

import by.pavel.che.dao.PurchaseDAO;
import by.pavel.che.dao.factory.DAOFactory;
import by.pavel.che.entity.Purchase;
import by.pavel.che.exception.service.ServiceException;
import by.pavel.che.service.PurchaseService;


import static by.pavel.che.util.caller.DAOCaller.tryCallDAO;

public class PurchaseServiceImpl implements PurchaseService {

    private PurchaseDAO getPurchaseDAO() {
        return DAOFactory.getDAOFactory().getPurchaseDAO();
    }

    @Override
    public Purchase add(Purchase purchase) throws ServiceException {
        return tryCallDAO(() -> getPurchaseDAO().insert(purchase));
    }
}
