package by.pavel.che.service.impl;

import by.pavel.che.dao.ProductDAO;
import by.pavel.che.dao.factory.DAOFactory;
import by.pavel.che.entity.Product;
import by.pavel.che.exception.service.ServiceException;
import by.pavel.che.service.ProductService;
import by.pavel.che.util.caller.DAOCaller;
import org.apache.log4j.Logger;

import java.util.List;

import static by.pavel.che.util.caller.DAOCaller.tryCallDAO;

public class ProductServiceImpl implements ProductService {

    private static final Logger logger = Logger.getLogger(ProductServiceImpl.class);

    private static int calculatePageCount(int purchaseCount, int itemCount) {
        int pageCount;
        if ((purchaseCount % itemCount == 0) && (purchaseCount != 0)) {
            pageCount = purchaseCount / itemCount;
        } else {
            pageCount = purchaseCount / itemCount + 1;
        }
        return pageCount;
    }

    private ProductDAO getProductDAO() {
        return DAOFactory.getDAOFactory().getProductDAO();
    }

    @Override
    public Product add(Product purchase) throws ServiceException {
        return tryCallDAO(() -> getProductDAO().insert(purchase));
    }

    @Override
    public Product getById(long id) throws ServiceException {
        return tryCallDAO(() -> getProductDAO().selectById(id));
    }


    @Override
    public void update(Product updatedPurchase) throws ServiceException {
        tryCallDAO(() -> getProductDAO().update(updatedPurchase));
    }

    @Override
    public void delete(long id) throws ServiceException {
        DAOCaller.tryCallDAO(() -> getProductDAO().delete(id));
    }

    @Override
    public ItemsPage<Product> getPage(int index, int itemsPerPage) throws ServiceException {
        if ((index - 1) * itemsPerPage < 0) {
            index = 1;
        }
        int offset = (index - 1) * itemsPerPage;
        List<Product> items = tryCallDAO(() -> getProductDAO().selectWithLimit(offset, itemsPerPage));
        int purchaseCount = tryCallDAO(getProductDAO()::selectCount);
        int pageCount = calculatePageCount(purchaseCount, itemsPerPage);
        return new ItemsPage<>(items, pageCount, index);
    }

    @Override
    public ItemsPage<Product> getPageForCategory(int index, int itemsPerPage, long catId) throws ServiceException {
        if ((index - 1) * itemsPerPage < 0) {
            index = 1;
        }
        int offset = (index - 1) * itemsPerPage;
        List<Product> items = tryCallDAO(() -> getProductDAO().selectForCategoryWithLimit(offset, itemsPerPage, catId));
        int purchaseCount = tryCallDAO(getProductDAO()::selectCount);
        int pageCount = calculatePageCount(purchaseCount, itemsPerPage);
        return new ItemsPage<>(items, pageCount, index);
    }

}
