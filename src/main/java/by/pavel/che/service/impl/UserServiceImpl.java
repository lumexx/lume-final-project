package by.pavel.che.service.impl;

import by.pavel.che.dao.UserDAO;
import by.pavel.che.dao.factory.DAOFactory;
import by.pavel.che.entity.User;
import by.pavel.che.exception.service.ServiceException;
import by.pavel.che.exception.service.UserServiceException;
import by.pavel.che.service.UserService;
import by.pavel.che.util.HashUtil;
import by.pavel.che.util.caller.DAOCaller;
import org.apache.log4j.Logger;

import java.util.List;

import static by.pavel.che.util.NullUtil.isNull;
import static by.pavel.che.util.StringUtil.isNullOrEmpty;
import static by.pavel.che.util.caller.DAOCaller.tryCallDAO;

public class UserServiceImpl implements UserService {
    private static final Logger logger = Logger.getLogger(UserServiceImpl.class);
    private static final String EMAIL_REGEX = "^([_a-zA-Z0-9-]+)(\\.[_a-zA-Z0-9-]+)*@([a-zA-Z0-9-]+\\.)+([a-zA-Z]{2,3})$";
    private static final String USERNAME_REGEX = "^[A-Za-z0-9]+(?:[ _-][A-Za-z0-9]+)*$";

    private static boolean isValidEmail(String email) {
        return !isNullOrEmpty(email) && email.matches(EMAIL_REGEX);
    }

    private static boolean isValidUsername(String username) {
        return !isNullOrEmpty(username) && username.matches(USERNAME_REGEX);
    }

    private UserDAO getUserDAO() {
        return DAOFactory.getDAOFactory().getUserDAO();
    }

    private static int calculatePageCount(int userCount, int itemCount) {
        int pageCount;
        if ((userCount % itemCount == 0) && (userCount != 0)) {
            pageCount = userCount / itemCount;
        } else {
            pageCount = userCount / itemCount + 1;
        }
        return pageCount;
    }

    @Override
    public User getById(long id) throws ServiceException {
        return tryCallDAO(() -> getUserDAO().selectById(id));
    }

    @Override
    public User register(String username, String email, String password) throws ServiceException {
        if (!isValidEmail(email)) {
            logger.debug("Invalid email format of \"" + email + "\"");
            throw new UserServiceException("Invalid email format");
        }
        if (!isValidUsername(username)) {
            logger.debug("Invalid username format of \"" + username + "\"");
            throw new UserServiceException("Invalid username format");
        }

        String passwordHash = HashUtil.hashString(password);
        User user;
        user = tryCallDAO(() -> getUserDAO().selectByUsername(username));
        if (!isNull(user)) {
            logger.debug("This username \"" + username + "\" is already exists.");
            throw new UserServiceException("User with that username is already exists");
        }

        user = tryCallDAO(() -> getUserDAO().selectByEmail(email));
        if (!isNull(user)) {
            logger.debug("This email \"" + email + "\" is already exists.");
            throw new UserServiceException("User with that email is already exists");
        }

        User newUser = new User();
        newUser.setName(username);
        newUser.setEmail(email);
        newUser.setPassword(passwordHash);

        return tryCallDAO(() -> getUserDAO().insert(newUser));
    }

    @Override
    public User login(String username, String password) throws ServiceException {
        if (isNullOrEmpty(username)) {
            logger.debug("Username is empty");
            throw new UserServiceException("Empty username passed.");
        }
        if (isNullOrEmpty(password))
        {
            logger.debug("Password is empty");
            throw new UserServiceException("Empty password passed.");
        }

        User user;
        user = tryCallDAO(() -> getUserDAO().selectByUsername(username));
        if (!isNull(user)) {
            if (!HashUtil.isValidHash(password, user.getPassword())) {
                logger.debug("Incorrect password for username \"" + username + "\"");
                throw new UserServiceException("Incorrect password.");
            }
        } else {
            logger.debug("User with username \"" + username + "\" doesn't exists.");
            throw new UserServiceException("Incorrect password.");
        }
        return user;
    }

    @Override
    public User getByUsername(String username) throws ServiceException {
        if (isNullOrEmpty(username)) {
            return null;
        }
        return tryCallDAO(() -> getUserDAO().selectByUsername(username));
    }

    @Override
    public void update(User updatedUser) throws ServiceException {
        tryCallDAO(() -> getUserDAO().update(updatedUser));
    }

    @Override
    public void delete(long id) throws ServiceException {
        DAOCaller.tryCallDAO(() -> getUserDAO().delete(id));
    }

    @Override
    public ItemsPage<User> getPage(int index, int itemsPerPage) throws ServiceException {
        if ((index - 1) * itemsPerPage < 0) {
            index = 1;
        }
        int offset = (index - 1) * itemsPerPage;
        List<User> items = tryCallDAO(() -> getUserDAO().selectWithLimit(offset, itemsPerPage));
        int userCount = tryCallDAO(getUserDAO()::selectCount);
        int pageCount = calculatePageCount(userCount, itemsPerPage);
        return new ItemsPage<>(items, pageCount, index);
    }
}
