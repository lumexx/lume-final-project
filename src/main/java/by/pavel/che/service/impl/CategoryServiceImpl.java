package by.pavel.che.service.impl;


import by.pavel.che.dao.CategoryDAO;
import by.pavel.che.dao.factory.DAOFactory;
import by.pavel.che.entity.Category;
import by.pavel.che.exception.service.ServiceException;
import by.pavel.che.service.CategoryService;

import java.util.List;

import static by.pavel.che.util.caller.DAOCaller.tryCallDAO;

public class CategoryServiceImpl implements CategoryService {

    private CategoryDAO getCategoryDAO() {
        return DAOFactory.getDAOFactory().getCategoryDAO();
    }

    @Override
    public List<Category> getAllCategories() throws ServiceException {
        return tryCallDAO(() -> getCategoryDAO().selectAllCategories());
    }

}
