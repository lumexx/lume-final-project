package by.pavel.che.service;

import by.pavel.che.entity.Product;
import by.pavel.che.exception.service.ServiceException;

/**
 * Service which provides all needed product methods.
 */
public interface ProductService extends ItemPageService<Product> {

    /**
     * Default products count on one page
     */
    int PRODUCTS_PER_PAGE = 9;

    /**
     * Adds new product
     *
     * @param product product
     * @return {@link Product} entity with all filled properties
     * @throws ServiceException       if error happens during execution
     */
    Product add(Product product) throws ServiceException;

    /**
     * Returns {@link Product} with the specified ID
     *
     * @param id id of an answer
     * @return {@link Product} with the specified ID, {@code null} if such answer is not exist
     * @throws ServiceException if error happens during execution
     */
    Product getById(long id) throws ServiceException;

    /**
     * Updates user data<br>
     *
     * @param updatedProduct updated user entity which will be stored into database
     * @throws ServiceException if error happens during execution
     */
    void update(Product updatedProduct) throws ServiceException;

    /**
     * Deletes product with the specified id from the system.<br>
     * Does nothing if user with this id doesn't exist.
     *
     * @param id of the {@link Product} to delete
     * @throws ServiceException if error happens during execution
     */
    void delete(long id) throws ServiceException;

    /**
     * Selects page of products for specific category id with specified index
     *
     * @param index        index of the page
     * @param itemsPerPage maximum count of products on the single page
     * @param catId  categoryId
     * @return {@link ItemsPage} entity of the current page
     * @throws ServiceException if error happens during execution
     */
    ItemsPage<Product> getPageForCategory(int index, int itemsPerPage, long catId) throws ServiceException;

}
