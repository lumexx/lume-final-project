package by.pavel.che.service.factory;

import by.pavel.che.service.*;
import by.pavel.che.service.impl.*;

/**
 * Provides access to service layer objects
 */
public class ServiceFactory {

    private static final ServiceFactory instance = new ServiceFactory();
    private UserService userService;
    private ProductService productService;
    private CategoryService categoryService;
    private PurchaseService purchaseService;
    private OrderService orderService;

    private ServiceFactory() {
        userService = new UserServiceImpl();
        productService = new ProductServiceImpl();
        categoryService = new CategoryServiceImpl();
        purchaseService = new PurchaseServiceImpl();
        orderService = new OrderServiceImpl();
    }

    public static ServiceFactory getInstance() {
        return instance;
    }

    public UserService getUserService() {
        return userService;
    }

    public CategoryService getCategoryService() {
        return categoryService;
    }

    public ProductService getProductService() {
        return productService;
    }

    public PurchaseService getPurchaseService() {
        return purchaseService;
    }

    public OrderService getOrderService() {
        return orderService;
    }
}
