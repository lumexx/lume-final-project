package by.pavel.che.service;

import by.pavel.che.entity.Purchase;
import by.pavel.che.exception.service.ServiceException;

/**
 * Service which provides all needed purchase methods.
 */
public interface PurchaseService {
    /**
     * Adds new purchase
     *
     * @param purchase purchase
     * @return {@link Purchase} entity with all filled properties
     * @throws ServiceException       if error happens during execution
     */
    Purchase add(Purchase purchase) throws ServiceException;
}
