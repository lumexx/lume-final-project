package by.pavel.che.dao.factory;

import by.pavel.che.dao.*;

/**
 * Provides basic access for DAO
 */
public abstract class DAOFactory {

    public static DAOFactory getDAOFactory() {
        DAOFactory result;
        result = MySqlDAOFactory.getInstance();
        return result;
    }

    /**
     * Returns concrete implementation of UserDAO for appropriate database
     *
     * @return concrete {@link UserDAO}
     */
    public abstract UserDAO getUserDAO();

    /**
     * Returns concrete implementation of OrderDAO for appropriate database
     *
     * @return concrete {@link OrderDAO}
     */
    public abstract OrderDAO getOrderDAO();

    /**
     * Returns concrete implementation of PurchaseDAO for appropriate database
     *
     * @return concrete {@link PurchaseDAO}
     */
    public abstract PurchaseDAO getPurchaseDAO();

    /**
     * Returns concrete implementation of ProductDAO for appropriate database
     *
     * @return concrete {@link ProductDAO}
     */
    public abstract ProductDAO getProductDAO();

    /**
     * Returns concrete implementation of CategoryDAO for appropriate database
     *
     * @return concrete {@link CategoryDAO}
     */
    public abstract CategoryDAO getCategoryDAO();
}