package by.pavel.che.dao.factory;

import by.pavel.che.dao.*;
import by.pavel.che.dao.impl.*;

public class MySqlDAOFactory extends DAOFactory {

    private static final MySqlDAOFactory instance = new MySqlDAOFactory();
    private ProductDAO productDAO;
    private UserDAO userDAO;
    private CategoryDAO categoryDAO;
    private PurchaseDAO purchaseDAO;
    private OrderDAO orderDAO;

    private MySqlDAOFactory() {
        productDAO = new MySqlProductDAO();
        userDAO = new MySqlUserDAO();
        categoryDAO = new MySqlCategoryDAO();
        purchaseDAO = new MySqlPurchaseDAO();
        orderDAO = new MySqlOrderDAO();
    }

    public static MySqlDAOFactory getInstance() {
        return instance;
    }

    @Override
    public UserDAO getUserDAO() {
        return userDAO;
    }

    @Override
    public OrderDAO getOrderDAO() { return orderDAO; }

    @Override
    public PurchaseDAO getPurchaseDAO() { return purchaseDAO; }

    @Override
    public ProductDAO getProductDAO() {
        return productDAO;
    }

    @Override
    public CategoryDAO getCategoryDAO() {
        return categoryDAO;
    }
}