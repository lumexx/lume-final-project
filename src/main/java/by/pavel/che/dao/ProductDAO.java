package by.pavel.che.dao;

import by.pavel.che.entity.Product;
import by.pavel.che.exception.dao.DAOException;

import java.util.List;

public interface ProductDAO extends BaseDAO<Product>, EntityListDAO<Product> {

    /**
     * Inserts mark into the database
     *
     * @param entity product
     * @return {@link  Product} full entity of inserted row
     * @throws DAOException if something went wrong
     */
    @Override
    Product insert(Product entity) throws DAOException;

    /**
     * Selects product from the database with specified ID
     *
     * @param id of an entity
     * @return - {@link  Product} with the specified id <br> - {@code null} if entity not found
     * @throws DAOException if something went wrong
     */
    @Override
    Product selectById(long id) throws DAOException;

    /**
     * Selects list of products with specified category and with limit constraints.<br>
     *
     * @param catId  id of category to select
     * @param offset offset of the first question in selected list
     * @param count  count of questions to select
     * @return list of {@link Product} with the specified category id
     * @throws DAOException if something went wrong
     */
    List<Product> selectForCategoryWithLimit(int offset, int count, long catId) throws DAOException;

    /**
     * Updates product in the database.
     *
     * @param updatedEntity entity to update
     * @throws DAOException if something went wrong
     */
    @Override
    void update(Product updatedEntity) throws DAOException;

    /**
     * Removes product with the specified id
     *
     * @param id id of a product to delete
     * @throws DAOException if something went wrong
     */
    @Override
    void delete(long id) throws DAOException;

}