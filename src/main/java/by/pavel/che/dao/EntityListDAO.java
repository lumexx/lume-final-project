package by.pavel.che.dao;

import by.pavel.che.entity.Entity;
import by.pavel.che.exception.dao.DAOException;

import java.util.List;

/**
 * Interface which provides base methods to wirk with lists of entities
 *
 * @param <E>
 */
public interface EntityListDAO<E extends Entity> {

    /**
     * Selects list of entities with limit constraints.<br>
     *
     * @param offset offset of the first entity in selected list
     * @param count  count of entities to select
     * @return list of {@link Entity} with the specified limit
     * @throws DAOException if something went wrong
     */
    List<E> selectWithLimit(int offset, int count) throws DAOException;

    /**
     * Selects count of all entities
     *
     * @return count of all entities
     * @throws DAOException if something went wrong
     */
    int selectCount() throws DAOException;
}