package by.pavel.che.dao;

import by.pavel.che.entity.Order;
import by.pavel.che.exception.dao.DAOException;

import java.util.List;

public interface OrderDAO extends BaseDAO<Order>, EntityListDAO<Order> {

    /**
     * Inserts mark into the database
     *
     * @param entity order
     * @return {@link  Order} full entity of inserted row
     * @throws DAOException if something went wrong
     */
    @Override
    Order insert(Order entity) throws DAOException;

    /**
     * Selects order from the database with specified ID
     *
     * @param id of an entity
     * @return - {@link  Order} with the specified id <br> - {@code null} if entity not found
     * @throws DAOException if something went wrong
     */
    @Override
    Order selectById(long id) throws DAOException;

    /**
     * Selects list of products with specified category and with limit constraints.<br>
     *
     * @param username username of user to select
     * @param offset   offset of the first question in selected list
     * @param count    count of questions to select
     * @return list of {@link Order} with the specified username
     * @throws DAOException if something went wrong
     */
    List<Order> selectByUsernameWithLimit(String username, int offset, int count) throws DAOException;

    /**
     * Selects count of all orders which belongs to user with the specified username
     *
     * @param username username of user
     * @return count of all orders which belongs to user with specified username
     * @throws DAOException if something went wrong
     */
    int selectCountByUsername(String username) throws DAOException;

    /**
     * Updates order in the database.
     *
     * @param updatedEntity entity to update
     * @throws DAOException if something went wrong
     */
    @Override
    void update(Order updatedEntity) throws DAOException;

    /**
     * Removes order with the specified id
     *
     * @param id id of a order to delete
     * @throws DAOException if something went wrong
     */
    @Override
    void delete(long id) throws DAOException;
}
