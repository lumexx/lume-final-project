package by.pavel.che.dao.impl;

import by.pavel.che.dao.UserDAO;
import by.pavel.che.entity.User;
import by.pavel.che.exception.dao.DAOException;
import org.apache.log4j.Logger;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import static by.pavel.che.util.caller.JDBCCaller.tryCallJDBC;

/**
 * MySQL implementation of {@link UserDAO}
 */
public class MySqlUserDAO extends AbstractBaseDAO<User> implements UserDAO {

    private static final Logger logger = Logger.getLogger(MySqlUserDAO.class);
    private static final String QUERY_SELECT_BY_ID = "SELECT `users`.`id`, `users`.`email`, `users`.`name`, `users`.`pwd`, `users`.`role`,`users`.`locale` FROM `users` WHERE `id` = ?;";
    private static final String QUERY_SELECT_BY_USERNAME = "SELECT `users`.`id`, `users`.`email`, `users`.`name`, `users`.`pwd`, `users`.`role`,`users`.`locale` FROM `users` WHERE (`name` = ?);";
    private static final String QUERY_SELECT_BY_EMAIL = "SELECT `users`.`id`, `users`.`email`, `users`.`name`, `users`.`pwd`, `users`.`role`,`users`.`locale` FROM `users` WHERE (`email` = ?);";
    private static final String QUERY_INSERT = "INSERT INTO `users` (`email`, `pwd`, `name`) VALUES (?, ?, ?)";
    private static final String QUERY_SELECT_COUNT = "SELECT COUNT(`id`) FROM `users`";
    private static final String QUERY_SELECT_WITH_LIMIT = "SELECT  `users`.`id`, `users`.`email`, `users`.`name`, `users`.`pwd`, `users`.`role`,`users`.`locale` FROM `users` GROUP BY `users`.`id` ORDER BY `users`.`id` DESC LIMIT ?, ?;";
    private static final String QUERY_UPDATE = "UPDATE `users` SET `name` = ?, `email` = ?, `role` = ?, `locale` = ? WHERE (`id` = ?);";
    private static final String QUERY_DELETE = "DELETE FROM `users` WHERE `id` = ?;";

    @Override
    public User insert(User entity) throws DAOException {
        return tryCallJDBC(QUERY_INSERT, ((connection, statement) -> {
            statement.setString(1, entity.getEmail());
            statement.setString(2, entity.getPassword());
            statement.setString(3, entity.getName());
            statement.executeUpdate();

            try (ResultSet keys = statement.getGeneratedKeys()) {
                if (keys.next()) {
                    long insertedId = keys.getLong(1);
                    return executeSelectById(connection, QUERY_SELECT_BY_ID, insertedId);
                } else {
                    throw new DAOException("Generated keys set is empty");
                }
            }
        }));
    }

    @Override
    public User selectById(long id) throws DAOException {
        return executeSelectById(QUERY_SELECT_BY_ID, id);
    }

    @Override
    public User selectByUsername(String username) throws DAOException {
        return selectWithStringParameter(QUERY_SELECT_BY_USERNAME, username);
    }

    @Override
    public User selectByEmail(String email) throws DAOException {
        return selectWithStringParameter(QUERY_SELECT_BY_EMAIL, email);
    }

    @Override
    public List<User> selectWithLimit(int offset, int count) throws DAOException {
        return tryCallJDBC(QUERY_SELECT_WITH_LIMIT, statement -> {
            statement.setInt(1, offset);
            statement.setInt(2, count);
            return executeStatementAndParseResultSetToList(statement);
        });
    }

    @Override
    public int selectCount() throws DAOException {
        return tryCallJDBC(QUERY_SELECT_COUNT, statement -> {
            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    return resultSet.getInt(1);
                } else {
                    logger.error("Cannot select user count, empty result set.");
                    throw new DAOException("Cannot select user count");
                }
            }
        });
    }


    @Override
    public void update(User updatedEntity) throws DAOException {
        tryCallJDBC(QUERY_UPDATE, ((connection, statement) -> {
            statement.setString(1, updatedEntity.getName());
            statement.setString(2, updatedEntity.getEmail());
            statement.setInt(3, updatedEntity.getRole());
            statement.setString(4, updatedEntity.getLocale());
            statement.setLong(5, updatedEntity.getId());
            statement.executeUpdate();
        }));
    }

    @Override
    public void delete(long id) throws DAOException {
        executeDelete(QUERY_DELETE, id);
    }

    private User selectWithStringParameter(String selectQuery, String parameter) throws DAOException {
        return tryCallJDBC(selectQuery, statement -> {
            statement.setString(1, parameter);
            return executeStatementAndParseResultSet(statement);
        });
    }

    @Override
    protected User parseResultSet(ResultSet resultSet) throws SQLException {
        User user = new User();
        user.setId(resultSet.getLong(1));
        user.setEmail(resultSet.getString(2));
        user.setName(resultSet.getString(3));
        user.setPassword(resultSet.getString(4));
        user.setRole(resultSet.getInt(5));
        return user;
    }
}