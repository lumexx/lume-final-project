package by.pavel.che.dao.impl;

import by.pavel.che.dao.OrderDAO;
import by.pavel.che.entity.Order;
import by.pavel.che.exception.dao.DAOException;
import org.apache.log4j.Logger;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import static by.pavel.che.util.caller.JDBCCaller.tryCallJDBC;

/**
 * MySQL implementation of {@link OrderDAO}
 */
public class MySqlOrderDAO extends AbstractBaseDAO<Order> implements OrderDAO {

    private static final Logger logger = Logger.getLogger(MySqlOrderDAO.class);
    private final static String QUERY_INSERT = "INSERT INTO `orders` (`user_id`, `status`) VALUES (?, ?);";
    private static final String QUERY_SELECT_BY_ID = "SELECT `orders`.`id`, `orders`.`user_id`, `orders`.`date_created`, `orders`.`date_payment`, `orders`.`date_modification`, `orders`.`status` FROM `orders` WHERE `id` = ?;";
    private static final String QUERY_SELECT_BY_USERNAME_WITH_LIMIT = "SELECT `orders`.`id`, `orders`.`user_id`, `orders`.`date_created`, `orders`.`date_payment`, `orders`.`date_modification`, `orders`.`status` FROM `orders` JOIN `users` ON `orders`.`user_id` = `users`.`id` WHERE (`users`.`name` = ?) GROUP BY `orders`.`id` ORDER BY `orders`.`id` DESC LIMIT ?, ?;";
    private static final String QUERY_SELECT_COUNT_BY_USERNAME = "SELECT COUNT(`orders`.`id`) FROM `orders` JOIN `users` ON `orders`.`user_id` = `users`.`id` WHERE (`users`.`name` = ?)";
    private static final String QUERY_SELECT_COUNT = "SELECT COUNT(`id`) FROM `orders`";

    @Override
    public Order insert(Order entity) throws DAOException {
        return tryCallJDBC(QUERY_INSERT, ((connection, statement) -> {
            statement.setLong(1, entity.getUserId());
            statement.setInt(2, entity.getStatus());
            statement.executeUpdate();

            try (ResultSet keys = statement.getGeneratedKeys()) {
                if (keys.next()) {
                    long insertedId = keys.getLong(1);
                    return executeSelectById(connection, QUERY_SELECT_BY_ID, insertedId);
                } else {
                    throw new DAOException("Generated keys set is empty");
                }
            }
        }));
    }

    @Override
    public Order selectById(long id) throws DAOException {
        return null;
    }

    @Override
    public List<Order> selectWithLimit(int offset, int count) throws DAOException {
        return null;
    }

    @Override
    public List<Order> selectByUsernameWithLimit(String username, int offset, int count) throws DAOException {
        return tryCallJDBC(QUERY_SELECT_BY_USERNAME_WITH_LIMIT, statement -> {
            statement.setString(1, username);
            statement.setInt(2, offset);
            statement.setInt(3, count);
            return executeStatementAndParseResultSetToList(statement);
        });
    }

    @Override
    public int selectCount() throws DAOException {
        return tryCallJDBC(QUERY_SELECT_COUNT, statement -> {
            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    return resultSet.getInt(1);
                } else {
                    logger.error("Cannot select order count, empty result set.");
                    throw new DAOException("Cannot select order count");
                }
            }
        });
    }

    @Override
    public int selectCountByUsername(String username) throws DAOException {
        return tryCallJDBC(QUERY_SELECT_COUNT_BY_USERNAME, statement -> {
            statement.setString(1, username);
            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    return resultSet.getInt(1);
                } else {
                    logger.error("Cannot select order count, empty result set.");
                    throw new DAOException("Cannot select order count");
                }
            }
        });
    }

    @Override
    public void update(Order updatedEntity) throws DAOException {

    }

    @Override
    public void delete(long id) throws DAOException {

    }

    @Override
    Order parseResultSet(ResultSet resultSet) throws SQLException {
        Order order = new Order();
        order.setId(resultSet.getLong(1));
        order.setUserId(resultSet.getLong(2));
        order.setDateCreated(resultSet.getTimestamp(3));
        order.setDatePayment(resultSet.getTimestamp(4));
        order.setDateModification(resultSet.getTimestamp(5));
        order.setStatus(resultSet.getInt(6));
        return order;
    }
}
