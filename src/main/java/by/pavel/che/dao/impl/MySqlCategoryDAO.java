package by.pavel.che.dao.impl;

import by.pavel.che.dao.CategoryDAO;
import by.pavel.che.entity.Category;
import by.pavel.che.exception.dao.DAOException;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import static by.pavel.che.util.caller.JDBCCaller.tryCallJDBC;

/**
 * MySQL implementation of {@link CategoryDAO}
 */
public class MySqlCategoryDAO extends AbstractBaseDAO<Category> implements CategoryDAO {

    private static final String QUERY_SELECT_ALL_CAT = "SELECT `categories`.id, `categories`.`name` FROM `categories`";

    @Override
    public Category insert(Category entity) throws DAOException {
        return null;
    }

    @Override
    public Category selectById(long id) throws DAOException {
        return null;
    }

    @Override
    public List<Category> selectAllCategories() throws DAOException {
        return tryCallJDBC(QUERY_SELECT_ALL_CAT, this::executeStatementAndParseResultSetToList);
    }

    @Override
    public void update(Category updatedEntity) throws DAOException {

    }

    @Override
    public void delete(long id) throws DAOException {
    }

    @Override
    Category parseResultSet(ResultSet resultSet) throws SQLException {
        Category category = new Category();
        category.setId(resultSet.getLong(1));
        category.setName(resultSet.getString(2));
        return category;
    }
}
