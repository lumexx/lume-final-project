package by.pavel.che.dao.impl;

import by.pavel.che.dao.PurchaseDAO;
import by.pavel.che.entity.Purchase;
import by.pavel.che.exception.dao.DAOException;

import java.sql.ResultSet;
import java.sql.SQLException;

import static by.pavel.che.util.caller.JDBCCaller.tryCallJDBC;

/**
 * MySQL implementation of {@link PurchaseDAO}
 */
public class MySqlPurchaseDAO extends AbstractBaseDAO<Purchase> implements PurchaseDAO {

    private final static String QUERY_INSERT = "INSERT INTO `purchase` (`order_id`, `product_id`,`price`,`amount`) VALUES (?, ?,?,?);";
    private static final String QUERY_SELECT_BY_ID = "SELECT `purchase`.`id`, `purchase`.`order_id`, `purchase`.`product_id`, `purchase`.`price`, `purchase`.`amount` FROM `purchase` WHERE `id` = ?;";


    @Override
    public Purchase insert(Purchase entity) throws DAOException {
        return tryCallJDBC(QUERY_INSERT, ((connection, statement) -> {
            statement.setLong(1, entity.getOrderId());
            statement.setLong(2, entity.getProductId());
            statement.setInt(3, entity.getPrice());
            statement.setInt(4, entity.getAmount());
            statement.executeUpdate();

            try (ResultSet keys = statement.getGeneratedKeys()) {
                if (keys.next()) {
                    long insertedId = keys.getLong(1);
                    return executeSelectById(connection, QUERY_SELECT_BY_ID, insertedId);
                } else {
                    throw new DAOException("Generated keys set is empty");
                }
            }
        }));
    }

    @Override
    public Purchase selectById(long id) throws DAOException {
        return null;
    }

    @Override
    public void update(Purchase updatedEntity) throws DAOException {

    }

    @Override
    public void delete(long id) throws DAOException {

    }

    @Override
    Purchase parseResultSet(ResultSet resultSet) throws SQLException {
        Purchase purchase = new Purchase();
        purchase.setId(resultSet.getLong(1));
        purchase.setOrderId(resultSet.getLong(2));
        purchase.setProductId(resultSet.getLong(3));
        purchase.setPrice(resultSet.getInt(4));
        purchase.setAmount(resultSet.getInt(5));
        return purchase;
    }
}
