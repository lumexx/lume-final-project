package by.pavel.che.dao.impl;

import by.pavel.che.dao.ProductDAO;
import by.pavel.che.entity.Product;
import by.pavel.che.exception.dao.DAOException;
import org.apache.log4j.Logger;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import static by.pavel.che.util.caller.JDBCCaller.tryCallJDBC;

/**
 * MySQL implementation of {@link ProductDAO}
 */
public class MySqlProductDAO extends AbstractBaseDAO<Product> implements ProductDAO {

    private static final Logger logger = Logger.getLogger(MySqlProductDAO.class);
    private static final String QUERY_SELECT_WITH_LIMIT = "SELECT `products`.`id`, `products`.`category_id`, `products`.`name`, `products`.`description`, `products`.`price`, `products`.`image`, `categories`.`name` FROM `products` JOIN `categories` ON `products`.`category_id` = `categories`.`id` GROUP BY `products`.`id` ORDER BY `products`.`id` DESC LIMIT ?, ?;";
    private static final String QUERY_SELECT_COUNT = "SELECT COUNT(`id`) FROM `products`";
    private static final String QUERY_SELECT_BY_ID = "SELECT `products`.`id`, `products`.`category_id`, `products`.`name`, `products`.`description`, `products`.`price`, `products`.`image`, `categories`.`name` FROM `products` JOIN `categories` ON `products`.`category_id` = `categories`.`id` WHERE (`products`.`id` = ?)";
    private static final String QUERY_SELECT_FOR_CAT_WITH_LIMIT = "SELECT `products`.`id`, `products`.`category_id`, `products`.`name`, `products`.`description`, `products`.`price`, `products`.`image`, `categories`.`name` FROM `products` JOIN `categories` ON `products`.`category_id` = `categories`.`id` WHERE (`products`.`category_id` = ?) GROUP BY `products`.`id` ORDER BY `products`.`id` DESC LIMIT ?, ?;";
    private static final String QUERY_UPDATE = "UPDATE `products` SET `category_id` = ?, `name` = ?, `description` = ?, `price` = ?, `image` = ? WHERE (`id` = ?);";
    private static final String QUERY_DELETE = "DELETE FROM `products` WHERE `id` = ?;";
    private static final String QUERY_INSERT = "INSERT INTO `products` (`category_id`, `name`, `description`,`price`,`image`) VALUES (?, ?, ?, ?, ?);";

    @Override
    public Product insert(Product entity) throws DAOException {
        return tryCallJDBC(QUERY_INSERT, (connection, statement) -> {
            statement.setLong(1, entity.getCategoryId());
            statement.setString(2, entity.getName());
            statement.setString(3, entity.getDescription());
            statement.setLong(4, entity.getPrice());
            statement.setString(5, entity.getImage());
            statement.executeUpdate();

            try (ResultSet keys = statement.getGeneratedKeys()) {
                if (keys.next()) {
                    long insertedId = keys.getLong(1);
                    return executeSelectById(connection, QUERY_SELECT_BY_ID, insertedId);
                } else {
                    throw new DAOException("Generated keys set is empty");
                }
            }
        });
    }

    @Override
    public Product selectById(long id) throws DAOException {
        return executeSelectById(QUERY_SELECT_BY_ID, id);
    }

    @Override
    public List<Product> selectWithLimit(int offset, int count) throws DAOException {
        return tryCallJDBC(QUERY_SELECT_WITH_LIMIT, statement -> {
            statement.setInt(1, offset);
            statement.setInt(2, count);
            return executeStatementAndParseResultSetToList(statement);
        });
    }

    @Override
    public List<Product> selectForCategoryWithLimit(int offset, int count, long catId) throws DAOException {
        return tryCallJDBC(QUERY_SELECT_FOR_CAT_WITH_LIMIT, statement -> {
            statement.setLong(1, catId);
            statement.setInt(2, offset);
            statement.setInt(3, count);
            return executeStatementAndParseResultSetToList(statement);
        });
    }

    @Override
    public int selectCount() throws DAOException {
        return tryCallJDBC(QUERY_SELECT_COUNT, statement -> {
            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    return resultSet.getInt(1);
                } else {
                    logger.error("Cannot select order count, empty result set.");
                    throw new DAOException("Cannot select order count");
                }
            }
        });
    }

    @Override
    public void update(Product updatedEntity) throws DAOException {
        tryCallJDBC(QUERY_UPDATE, ((connection, statement) -> {
            statement.setLong(1, updatedEntity.getCategoryId());
            statement.setString(2, updatedEntity.getName());
            statement.setString(3, updatedEntity.getDescription());
            statement.setLong(4, updatedEntity.getPrice());
            statement.setString(5, updatedEntity.getImage());
            statement.setLong(6, updatedEntity.getId());
            statement.executeUpdate();
        }));
    }

    @Override
    public void delete(long id) throws DAOException {
        executeDelete(QUERY_DELETE, id);
    }

    @Override
    protected Product parseResultSet(ResultSet resultSet) throws SQLException {
        Product product = new Product();
        product.setId(resultSet.getLong(1));
        product.setCategoryId(resultSet.getLong(2));
        product.setName(resultSet.getString(3));
        product.setDescription(resultSet.getString(4));
        product.setPrice(resultSet.getLong(5));
        product.setImage(resultSet.getString(6));
        product.setCategoryName(resultSet.getString(7));
        return product;
    }

}