package by.pavel.che.dao;

import by.pavel.che.entity.User;
import by.pavel.che.exception.dao.DAOException;

public interface UserDAO extends BaseDAO<User>, EntityListDAO<User> {

    /**
     * Inserts user into the database
     *
     * @param entity user
     * @return {@link User} full entity of inserted row
     * @throws DAOException if something went wrong
     */
    @Override
    User insert(User entity) throws DAOException;

    /**
     * Selects user from the database with specified ID
     *
     * @param id of an entity
     * @return - {@link User} with the specified id <br> - {@code null} if entity not found
     * @throws DAOException if something went wrong
     */
    @Override
    User selectById(long id) throws DAOException;

    /**
     * Selects user from the database with specified username
     *
     * @param username of an entity
     * @return - {@link User} with the specified username <br> - {@code null} if entity not found
     * @throws DAOException if something went wrong
     */
    User selectByUsername(String username) throws DAOException;

    /**
     * Selects user from the database with specified email
     *
     * @param email of an entity
     * @return - {@link User} with the specified email <br> - {@code null} if entity not found
     * @throws DAOException if something went wrong
     */
    User selectByEmail(String email) throws DAOException;

    /**
     * Updates user in the database.
     *
     * @param updatedEntity entity to update
     * @throws DAOException if something went wrong
     */
    @Override
    void update(User updatedEntity) throws DAOException;

    /**
     * Deletes the user with the specified id
     *
     * @param id id of an entity to delete
     * @throws DAOException if something went wrong
     */
    @Override
    void delete(long id) throws DAOException;

}
