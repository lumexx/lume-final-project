package by.pavel.che.dao;

import by.pavel.che.entity.Purchase;
import by.pavel.che.exception.dao.DAOException;

public interface PurchaseDAO extends BaseDAO<Purchase> {

    /**
     * Inserts mark into the database
     *
     * @param entity purchase
     * @return {@link  Purchase} full entity of inserted row
     * @throws DAOException if something went wrong
     */
    @Override
    Purchase insert(Purchase entity) throws DAOException;

    /**
     * Selects purchase from the database with specified ID
     *
     * @param id of an entity
     * @return - {@link Purchase} with the specified id <br> - {@code null} if entity not found
     * @throws DAOException if something went wrong
     */
    @Override
    Purchase selectById(long id) throws DAOException;

    /**
     * Updates purchase in the database.
     *
     * @param updatedEntity entity to update
     * @throws DAOException if something went wrong
     */
    @Override
    void update(Purchase updatedEntity) throws DAOException;

    /**
     * Removes purchase with the specified id
     *
     * @param id id of a purchase to delete
     * @throws DAOException if something went wrong
     */
    @Override
    void delete(long id) throws DAOException;
}
