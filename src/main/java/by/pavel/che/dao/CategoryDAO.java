package by.pavel.che.dao;

import by.pavel.che.entity.Category;
import by.pavel.che.exception.dao.DAOException;

import java.util.List;

public interface CategoryDAO extends BaseDAO<Category> {

    /**
     * Inserts mark into the database
     *
     * @param entity category
     * @return {@link  Category} full entity of inserted row
     * @throws DAOException if something went wrong
     */
    @Override
    Category insert(Category entity) throws DAOException;

    /**
     * Selects category from the database with specified ID
     *
     * @param id of an entity
     * @return - {@link  Category) with the specified id <br> - {@code null} if entity not found
     * @throws DAOException if something went wrong
     */
    @Override
    Category selectById(long id) throws DAOException;

    /**
     * Selects all categories from database
     *
     * @return - {@link  Category)
     * @throws DAOException if something went wrong
     */
    List<Category> selectAllCategories() throws DAOException;

    /**
     * Updates category in the database.
     *
     * @param updatedEntity entity to update
     * @throws DAOException if something went wrong
     */
    @Override
    void update(Category updatedEntity) throws DAOException;

    /**
     * Removes question with the specified id
     *
     * @param id id of a category to delete
     * @throws DAOException if something went wrong
     */
    @Override
    void delete(long id) throws DAOException;

}
