-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: localhost    Database: myshop
-- ------------------------------------------------------
-- Server version	5.7.17-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (4,'PC'),(3,'XBOX'),(1,'PLAYSTATION'),(2,'WII-U');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `date_payment` datetime DEFAULT NULL,
  `date_modification` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=87 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders`
--

LOCK TABLES `orders` WRITE;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
INSERT INTO `orders` VALUES (1,10,'2019-04-24 23:51:51',NULL,'2019-04-24 21:51:51',0),(2,10,'2019-04-25 00:09:12',NULL,'2019-04-24 22:09:12',0),(3,10,'2019-04-25 01:01:11',NULL,'2019-04-24 23:01:11',0),(4,11,'2019-04-25 01:05:42','1230-00-00 00:00:00','2019-04-25 02:16:11',0),(5,10,'2019-04-25 10:51:41',NULL,'2019-04-25 08:51:41',0),(6,10,'2019-05-01 20:40:01',NULL,'2019-05-01 18:40:01',0),(7,10,'2019-05-02 00:15:51',NULL,'2019-05-01 22:15:51',0),(8,10,'2019-05-02 00:18:43',NULL,'2019-05-01 22:18:43',0),(9,10,'2019-05-02 00:22:58',NULL,'2019-05-01 22:22:58',0),(10,10,'2019-05-02 02:26:11',NULL,'2019-05-02 00:26:11',0),(11,10,'2019-05-02 02:27:11',NULL,'2019-05-02 00:27:11',0),(12,10,'2019-05-02 02:41:07',NULL,'2019-05-02 00:41:07',0),(13,10,'2019-05-02 03:53:50',NULL,'2019-05-02 01:53:50',0),(14,10,'2019-05-04 18:57:14',NULL,'2019-05-04 16:57:14',0),(15,10,'2019-05-04 19:40:07',NULL,'2019-05-04 17:40:07',0),(16,10,'2019-05-05 13:10:46',NULL,'2019-05-05 11:10:46',0),(17,10,'2019-05-05 13:11:17',NULL,'2019-05-05 11:11:17',0),(18,10,'2019-05-05 13:17:46',NULL,'2019-05-05 11:17:46',0),(19,10,'2019-05-05 13:18:02',NULL,'2019-05-05 11:18:02',0),(20,10,'2019-05-05 13:19:21',NULL,'2019-05-05 11:19:21',0),(21,10,'2019-05-05 13:21:40',NULL,'2019-05-05 11:21:40',0),(22,10,'2019-05-05 13:31:48',NULL,'2019-05-05 11:31:48',0),(23,10,'2019-05-05 13:33:05',NULL,'2019-05-05 11:33:05',0),(24,10,'2019-05-05 13:33:15',NULL,'2019-05-05 11:33:15',0),(25,10,'2019-05-05 13:35:09',NULL,'2019-05-05 11:35:09',0),(26,10,'2019-05-05 13:44:50',NULL,'2019-05-05 11:44:50',0),(27,10,'2019-05-05 13:45:05',NULL,'2019-05-05 11:45:05',0),(28,10,'2019-05-05 13:51:51',NULL,'2019-05-05 11:51:51',0),(29,10,'2019-05-05 13:54:49',NULL,'2019-05-05 11:54:49',0),(30,10,'2019-05-05 13:57:10',NULL,'2019-05-05 11:57:10',0),(31,10,'2019-05-05 13:57:46',NULL,'2019-05-05 11:57:46',0),(32,10,'2019-05-05 14:00:47',NULL,'2019-05-05 12:00:47',0),(33,10,'2019-05-05 14:06:58',NULL,'2019-05-05 12:06:58',0),(34,10,'2019-05-05 14:32:53',NULL,'2019-05-05 12:32:53',0),(35,10,'2019-05-05 14:40:53',NULL,'2019-05-05 12:40:53',0),(36,10,'2019-05-05 18:42:30',NULL,'2019-05-05 16:42:30',0),(37,10,'2019-05-05 19:24:53',NULL,'2019-05-05 17:24:53',0),(38,10,'2019-05-05 20:28:33',NULL,'2019-05-05 18:28:33',0),(39,10,'2019-08-03 13:11:58',NULL,'2019-08-03 10:11:58',0),(40,20,'2019-08-03 13:20:21',NULL,'2019-08-03 10:20:21',0),(41,20,'2019-08-03 13:27:25',NULL,'2019-08-03 10:27:25',0),(42,20,'2019-08-03 14:00:00',NULL,'2019-08-03 11:00:00',0),(43,20,'2019-08-03 14:12:49',NULL,'2019-08-03 11:12:49',0),(44,20,'2019-08-03 14:21:35',NULL,'2019-08-03 11:21:35',0),(45,20,'2019-08-03 14:26:35',NULL,'2019-08-03 11:26:35',0),(46,20,'2019-08-03 14:26:58',NULL,'2019-08-03 11:26:58',0),(47,20,'2019-08-03 14:30:16',NULL,'2019-08-03 11:30:16',0),(48,20,'2019-08-03 14:35:04',NULL,'2019-08-03 11:35:04',0),(49,20,'2019-08-03 14:36:27',NULL,'2019-08-03 11:36:27',0),(50,20,'2019-08-08 23:28:46',NULL,'2019-08-08 20:28:46',0),(51,20,'2019-08-09 15:04:52',NULL,'2019-08-09 12:04:52',0),(52,20,'2019-08-09 15:05:17',NULL,'2019-08-09 12:05:17',0),(53,20,'2019-08-10 13:30:40',NULL,'2019-08-10 10:30:40',0),(54,20,'2019-08-10 21:32:26',NULL,'2019-08-10 18:32:26',0),(55,20,'2019-08-10 21:32:31',NULL,'2019-08-10 18:32:31',0),(56,20,'2019-08-10 21:32:34',NULL,'2019-08-10 18:32:34',0),(57,20,'2019-08-10 21:32:47',NULL,'2019-08-10 18:32:47',0),(58,20,'2019-08-11 00:23:43',NULL,'2019-08-10 21:23:43',0),(59,30,'2019-08-11 00:39:19',NULL,'2019-08-10 21:39:19',0),(60,30,'2019-08-11 00:41:31',NULL,'2019-08-10 21:41:31',0),(61,20,'2019-08-11 00:41:41',NULL,'2019-08-10 21:41:41',0),(62,20,'2019-08-11 00:42:46',NULL,'2019-08-10 21:42:46',0),(63,20,'2019-08-12 10:40:39',NULL,'2019-08-12 07:40:39',0),(64,20,'2019-08-12 11:13:39',NULL,'2019-08-12 08:13:39',0),(65,20,'2019-08-12 11:13:50',NULL,'2019-08-12 08:13:50',0),(66,20,'2019-08-12 11:15:28',NULL,'2019-08-12 08:15:28',0),(67,20,'2019-08-12 11:19:46',NULL,'2019-08-12 08:19:46',0),(68,20,'2019-08-12 11:19:53',NULL,'2019-08-12 08:19:53',0),(69,20,'2019-08-12 11:20:02',NULL,'2019-08-12 08:20:02',0),(70,20,'2019-08-12 11:20:05',NULL,'2019-08-12 08:20:05',0),(71,20,'2019-08-12 11:20:21',NULL,'2019-08-12 08:20:21',0),(72,20,'2019-08-12 11:22:58',NULL,'2019-08-12 08:22:58',0),(73,20,'2019-08-12 11:23:02',NULL,'2019-08-12 08:23:02',0),(74,20,'2019-08-12 11:23:03',NULL,'2019-08-12 08:23:03',0),(75,20,'2019-08-12 11:23:04',NULL,'2019-08-12 08:23:04',0),(76,20,'2019-08-12 11:23:05',NULL,'2019-08-12 08:23:05',0),(77,20,'2019-08-12 11:28:00',NULL,'2019-08-12 08:28:00',0),(78,20,'2019-08-12 11:31:26',NULL,'2019-08-12 08:31:26',0),(79,20,'2019-08-12 11:33:49',NULL,'2019-08-12 08:33:49',0),(80,20,'2019-08-12 11:36:18',NULL,'2019-08-12 08:36:18',0),(81,20,'2019-08-12 12:42:40',NULL,'2019-08-12 09:42:40',0),(82,20,'2019-08-12 12:44:51',NULL,'2019-08-12 09:44:51',0),(83,20,'2019-08-13 19:33:28',NULL,'2019-08-13 16:33:28',0),(84,20,'2019-08-13 19:33:47',NULL,'2019-08-13 16:33:47',0),(85,20,'2019-08-13 19:34:20',NULL,'2019-08-13 16:34:20',0),(86,31,'2019-08-13 19:35:52',NULL,'2019-08-13 16:35:52',0);
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `price` float NOT NULL,
  `image` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products`
--

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` VALUES (1,3,'Assasins Creed 2','                        Assassin’s Creed II — культовая мультиплатформенная компьютерная игра в жанре action-adventure, продолжение игры Assassin’s Creed от компании Ubisoft. Официальный анонс состоялся 16 апреля 2009 года в журнале Game Informer.\r\n                ',3000,'%D1%8D%D1%86%D0%B8%D0%BE-%D0%B0%D1%83%D0%B4%D0%B8%D1%82%D0%BE%D1%80%D0%B5-%D0%B8%D0%B7-assassin%E2%80%99s-creed-ii.jpg'),(15,5,'FL Studio','FL Studio — цифровая звуковая рабочая станция и секвенсер для написания музыки. Музыка создаётся путём записи и сведения аудио- или MIDI-материала. Готовая композиция может быть записана в формате WAV, FLAC, MP3 или OGG. ',3000,'15.jpg'),(3,3,'Half-life 2','Компьютерная игра, научно-фантастический шутер от первого лица, сиквел Half-Life, разработанный Valve Corporation, издаваемый ими же и Sierra Entertainment. Игра была выпущена 16 ноября 2004 года (21 ноября 2004 года в России и странах СНГ компанией SoftClub) и получила высочайшие оценки многих авторитетных изданий',5000,'270px-HL2box.jpg'),(17,5,'Adobe Photoshop','Adobe Photoshop — многофункциональный графический редактор, разработанный и распространяемый фирмой Adobe Systems. В основном работает с растровыми изображениями, однако имеет некоторые векторные инструменты.',4325,'17.png'),(2,3,'Dota 2','Компьютерная многопользовательская командная игра в жанре multiplayer online battle arena, разработанная Valve Corporation. Игра является продолжением DotA — пользовательской карты-модификации для игры Warcraft III: Reign of Chaos и дополнения к ней Warcraft III: The Frozen Throne.',7000,'dota-2-patch-notes-7.21b-valve-steam.jpg'),(4,3,'Counter Strike 1.6','Counter-Strike — серия компьютерных игр в жанре командного шутера от первого лица, основанная на движке GoldSrc и выросшая из одноимённой модификации игры Half-Life',6000,'p1_2468313_ac9e4b38.png'),(16,6,'Gfinity','                        Переведено с английского языка.-Gfinity, основанная в 2012 году, является британской киберспортивной компанией, работающей по всему миру. Он базируется в Лондоне на единственной киберспортивной арене в Великобритании и котируется на Лондонской фондовой бирже, торгующей на рынке альтернативных инвестиций LON: GFIN.\n                ',3000,'16.jpg'),(5,3,'Dishonored','Компьютерная игра в жанре стелс-экшен от первого лица с элементами RPG, разработанная французской компанией Arkane Studios и изданная Bethesda Softworks. Игра вышла на PC, PlayStation 3 и Xbox 360 в 2012 году.',10000,'81ndRpxiYAL.jpg'),(6,3,'Max Payne','Компьютерная игра в жанре шутера от третьего лица, разработанная финской компанией Remedy Entertainment, спродюсированная 3D Realms и изданная Gathering of Developers в 2001 году. Это первая часть в серии Max Payne.',12000,'183202.jpg'),(7,3,'Devil May Cry','Серия из шести игр в жанре слэшер, разработанных компанией Capcom. Первая игра в серии задумывалась как продолжение Resident Evil, но в конечном итоге была выделена в отдельный проект. ',11000,'20181207211557_452d4a4c.jpg'),(8,3,'Doom','Cерия компьютерных игр в жанре шутера от первого лица, созданная компанией id Software. Игры серии рассказывает о подвигах неназванного космического пехотинца, работающего на Объединённую Аэрокосмическую Корпорацию и сражающегося против полчищ демонов, для того чтобы выжить и спасти Землю от их нападения.',15000,'H2x1_NSwitch_Doom_image1600w.jpg'),(12,4,'Ashen','Ashen — компьютерная игра в жанре Action/RPG, разработанная новозеландской студией А44 и выпущенная компанией Annapurna Interactive для Windows и Xbox One 7 декабря 2018 года. Действие Ashen происходит в фэнтезийном мире, лишенном солнца — с исчезновением божественной птицы по имени Пепельная мир погрузился во тьму.',25000,'15444207581c4883229fa87968f609aa4871cb32bbd.jpg'),(26,2,'Darkest Dungeon','Darkest Dungeon — компьютерная ролевая игра с элементами roguelike, разработанная и выпущенная независимой студией Red Hook Studios.',3245,'H2x1_NSwitchDS_DarkestDungeon_image1600w.jpg'),(18,5,'Xampp','                        XAMPP — кроссплатформенная сборка веб-сервера, содержащая Apache, MySQL, интерпретатор скриптов PHP, язык программирования Perl и большое количество дополнительных библиотек, позволяющих запустить полноценный веб-сервер.\n                ',1234,'18.jpg'),(20,4,'Tomb Raider','                        серия мультиплатформенных компьютерных игр в жанре action-adventure. Серия насчитывает 12 основных игр, первая из которых вышла в 1996 году, а последняя на данный момент часть, Shadow of the Tomb Raider, вышла 14 сентября 2018 года. Игры серии рассказывают о приключениях молодого британского археолога Лары Крофт. Кроме игр, по франшизе выпущено множество фильмов, комиксов и книг.\r\n                ',5555,'1200x630bb.png'),(21,4,'Mortal Kombat','Mortal Kombat — серия видеоигр в жанре файтинг, созданная Эдом Буном и Джоном Тобиасом. Игры этой серии изначально разрабатывались Midway Games для аркадных автоматов, а впоследствии были перенесены на другие платформы. Портированием на домашние игровые консоли занималась Acclaim Entertainment.',3355,'mortal-kombat-x-enhanced-online-beta-header.0.0.jpg'),(22,4,'FIFA 19','FIFA 19 — 26-я футбольная игра из серии игр FIFA, разработанная для платформ Windows, Nintendo Switch, PlayStation 4, PlayStation 3, Xbox One и Xbox 360. Игра выпущена компанией Electronic Arts 28 сентября 2018. Лицом игры является футболист Ювентуса и сборной Португалии Криштиану Роналду.',3342,'Italy_and_Mediterranean_node_full_image_2.jpg'),(23,3,'Toribash','Toribash — компьютерная игра, пошаговый файтинг, основанный на физике ragdoll. Она была создана шведским программистом-одиночкой Хампусом Сёдерстрёмом, позже основавшим компанию Nabi Studios в Сингапуре. Toribash является финалистом Independent Games Festival 2007 года в номинации «Инновационный дизайн».',9999,'screen2.jpg'),(24,3,'Apex Legends','Apex Legends — компьютерная игра в жанре многопользовательского шутера от первого лица и королевской битвы, разработанная американской студией Respawn Entertainment и выпущенная компанией Electronic Arts для платформ Windows, PlayStation 4 и Xbox One в 2019 году.',13243,'apex-featured-image-16x9.jpg.adapt.crop191x100.1200w.jpg'),(25,3,'Mafia II','ОписаниеMafia II — компьютерная игра в жанре приключенческого боевика с открытым миром, сочетающего в себе автомобильный симулятор и шутер с видом от третьего лица, разработанная чешской компанией 2K Czech; вторая игра серии Mafia.',12343,'sWCJwEQCEbQZbUtu2CHNQE.png');
/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `purchase`
--

DROP TABLE IF EXISTS `purchase`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `purchase` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `price` float NOT NULL,
  `amount` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=114 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `purchase`
--

LOCK TABLES `purchase` WRITE;
/*!40000 ALTER TABLE `purchase` DISABLE KEYS */;
INSERT INTO `purchase` VALUES (1,2,12,25000,1),(2,3,3,5000,1),(3,3,4,6000,5),(4,3,8,15000,1),(5,4,7,11000,1),(6,5,3,5000,1),(7,6,4,6000,1),(8,6,8,15000,1),(9,7,23,9999,1),(10,8,1,3000,1),(11,9,13,30,1),(12,10,24,13243,1),(13,11,3,5000,1),(14,12,20,5555,1),(15,12,23,9999,1),(16,13,23,9999,1),(17,13,25,2222,1),(18,14,17,4325,1),(19,15,6,12000,1),(20,15,20,5555,1),(21,15,21,3355,1),(22,15,25,2222,1),(23,16,20,5555,1),(24,17,1,3000,1),(25,17,23,9999,345),(26,18,18,1234,1),(27,19,18,1234,1),(28,20,18,1234,1),(29,21,20,5555,1),(30,22,20,5555,1),(31,23,18,1234,1),(32,24,23,9999,1),(33,25,23,9999,1),(34,26,16,3000,1),(35,27,20,5555,1),(36,28,23,9999,1),(37,29,17,4325,1),(38,30,22,3342,1),(39,31,18,1234,1),(40,32,23,9999,1),(41,33,17,4325,1),(42,34,6,12000,1),(43,34,13,30,1),(44,34,18,1234,1),(45,34,20,5555,1),(46,35,20,5555,1),(47,36,18,1234,1),(48,36,23,9999,1),(49,37,20,5555,1),(50,38,17,4325,1),(51,44,25,2222,2),(52,45,25,2222,1),(53,46,25,2222,3),(54,47,24,13243,2),(55,48,25,2222,1),(56,48,24,13243,2),(57,48,23,9999,1),(58,49,25,2222,1),(59,49,24,13243,4),(60,49,23,9999,2),(61,49,18,1234,1),(62,51,26,3245,2),(63,51,25,1234,2),(64,51,24,13243,1),(65,52,26,3245,2),(66,52,25,1234,3),(67,52,24,13243,1),(68,53,25,1234,1),(69,53,24,13243,1),(70,53,19,6676,1),(71,54,26,3245,2),(72,54,25,12343,1),(73,55,26,3245,2),(74,55,25,12343,1),(75,56,26,3245,2),(76,56,25,12343,1),(77,57,26,3245,2),(78,57,25,12343,1),(79,58,26,3245,2),(80,58,25,12343,1),(81,58,24,13243,1),(82,58,20,5555,1),(83,58,19,6676,1),(84,62,26,3245,1),(85,63,26,3245,1),(86,64,25,12343,1),(87,64,24,13243,1),(88,65,25,12343,1),(89,65,24,13243,1),(90,66,25,12343,1),(91,66,24,13243,1),(92,67,25,12343,1),(93,68,25,12343,1),(94,69,25,12343,1),(95,70,25,12343,1),(96,71,25,12343,1),(97,72,25,12343,1),(98,73,25,12343,1),(99,74,25,12343,1),(100,75,25,12343,1),(101,76,25,12343,1),(102,77,26,3245,1),(103,78,25,12343,1),(104,79,25,12343,1),(105,80,26,3245,1),(106,81,25,12343,1),(107,82,25,12343,1),(108,83,25,12343,2),(109,84,25,12343,1),(110,84,26,3245,1),(111,85,26,3245,2),(112,86,24,13243,1),(113,86,25,12343,2);
/*!40000 ALTER TABLE `purchase` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(50) NOT NULL,
  `pwd` varchar(100) NOT NULL,
  `name` varchar(50) NOT NULL,
  `role` tinyint(4) NOT NULL DEFAULT '0',
  `locale` varchar(45) DEFAULT 'ru',
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (26,'arminvanburen@gmail.com','$2a$12$gqTdPZPDnBxsJWTCpighLO3gh1Dh5gfg9qzkWLOJqiiNb/5G/GcyC','arminvanfunhrer',0,NULL),(19,'pavel33@gmail.com','$2a$12$qbx.SRdf835/yqWSjr8ryu5NHfNgkJ6UDgyJZ3M3zl35n5PqvJLhG','Fahrenheit',0,NULL),(20,'pavlik.1998@gmail.com','$2a$12$NTq290vVOrHlTGisaTFM/.E1uSaRXQu..DpTiw7woUQGGZR5b7f0O','Lumex',1,'en'),(21,'router@gmail.com','$2a$12$FaGdAPhvGiRTb8bJs5yF7O4uoTrswap54B3WEV6gYkcoHAPZrStsi','router',0,'ru'),(22,'username@gmail.com','$2a$12$dNotsgWQCE11s5x/npVt..XQW3d93xj2BpCzdn.wDm4tRIq5IBsfW','deadoutside',0,NULL),(24,'qwert@gmail.com','$2a$12$nMpkrFmoIv64T4umU1znceUDqL.6E7bCo/G2guuaDVmCceIuxtEPu','qwerty',0,'ru'),(25,'Prostor@gmail.com','$2a$12$Hkm9UrgCayUqIErTk2DD8eEVkva.TEi5Hw57ZnYACXf1hdEI1XWrO','Prostor',1,NULL),(27,'legolas@gmail.com','$2a$12$YImJ1iffary9AzFWtdS82uZTK/z4FMlTPZxT5rJn684wMue3usAC.','legolas',0,'ru'),(28,'ranmon@gmail.com','$2a$12$0LBGKObvQ3PD0bI0G3CaZuydnQCEKwTGCTsO.y0Y.MqYAoVsdrpFC','deadinside',0,NULL),(29,'noname@gmail.com','$2a$12$7AgmfXvNwbrAp7WCKZZvjuZRbWaICTg59df2u8.pjWGIesZqpI.Ga','dendi',0,NULL),(30,'lowskill@gmail.com','$2a$12$ZJxPXFM7YcvmUIbU9cZQv.oX3k8EbvZxa9WBJp0lUpEvoiqWPzRDC','lowskill',0,'ru'),(31,'kuroky@gmail.com','$2a$12$Sq92GQqN8CVclXmItIXkZ.cE1ZAuW/p3Ilpdz9qhkzM7euUbOqqpi','kuroky',0,'ru');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-08-13 20:36:05
